from rest_framework import serializers

from user.serializers import UserSerializer
from restaurant.serializers import RestaurantShortSerializer, RestaurantSerializer

from review.models import Review


class ReviewSerializer(serializers.ModelSerializer):
    comments = serializers.SerializerMethodField()

    def get_comments(self, obj):
        # Local import here to avoid circular dependency
        from reviewcomment.serializers import ReviewCommentShortSerializer
        comments = ReviewCommentShortSerializer(obj.review_comments.all(), many=True, context=self.context)
        return comments.data

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        request = self.context.get('request')
        representation['user'] = UserSerializer(instance.user, context={'request': request}).data
        representation['restaurant'] = RestaurantShortSerializer(instance.restaurant, context={'request': request}).data
        return representation

    class Meta:
        model = Review
        fields = [
            "id",
            "text",
            "rating",
            "likes_count",
            "user",
            "restaurant",
            "comments",
            "created_date",
        ]
        extra_kwargs = {
            'id': {'read_only': True},
            'user': {'read_only': True},
            'restaurant': {'read_only': True},
            'likes_count': {'read_only': True},
            'text': {'required': True},
            'rating': {'required': True},
            'comments': {'read_only': True},
        }


class ReviewAndUserSerializer(serializers.ModelSerializer):
    comments = serializers.SerializerMethodField()

    def get_comments(self, obj):
        # Local import here to avoid circular dependency
        from reviewcomment.serializers import ReviewCommentShortSerializer
        comments = ReviewCommentShortSerializer(obj.review_comments.all(), many=True, context=self.context)
        return comments.data

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        request = self.context.get('request')
        representation['user'] = UserSerializer(instance.user, context={'request': request}).data
        return representation

    class Meta:
        model = Review
        fields = [
            "id",
            "text",
            "rating",
            "likes_count",
            "user",
            "restaurant",
            "comments",
            "created_date",
        ]
        extra_kwargs = {
            'id': {'read_only': True},
            'user': {'read_only': True},
            'restaurant': {'read_only': True},
            'likes_count': {'read_only': True},
            'comments': {'read_only': True},
            'text': {'required': True},
            'rating': {'required': True},
        }


class ReviewAndRestaurantSerializer(serializers.ModelSerializer):
    comments = serializers.SerializerMethodField()

    def get_comments(self, obj):
        # Local import here to avoid circular dependency
        from reviewcomment.serializers import ReviewCommentShortSerializer
        comments = ReviewCommentShortSerializer(obj.review_comments.all(), many=True, context=self.context)
        return comments.data

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        request = self.context.get('request')
        representation['restaurant'] = RestaurantSerializer(instance.restaurant, context={'request': request}).data
        return representation

    class Meta:
        model = Review
        fields = [
            "id",
            "text",
            "rating",
            "likes_count",
            "user",
            "restaurant",
            "comments",
            "created_date",
        ]
        extra_kwargs = {
            'id': {'read_only': True},
            'user': {'read_only': True},
            'restaurant': {'read_only': True},
            'likes_count': {'read_only': True},
            'comments': {'read_only': True},
            'text': {'required': True},
            'rating': {'required': True},
        }
