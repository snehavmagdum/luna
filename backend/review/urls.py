from django.urls import path
from review import views

urlpatterns = [

    # List all reviews
    path('reviews/', views.ListRestaurantReviewsView.as_view()),

    # Create new review for a restaurant
    path('reviews/new/<int:restaurant_id>', views.CreateRestaurantReviewView.as_view()),

    # Get the list of the reviews for a single restaurant
    path('reviews/restaurant/<int:restaurant_id>', views.ListRestaurantReviewView.as_view()),

    # Get the list of the reviews by a single user
    path('reviews/user/<int:user_id>', views.ListUserReviewView.as_view()),

    # Retrieve, update, delete a specific review
    path('reviews/<int:pk>', views.RetrieveUpdateDeleteReviewView.as_view()),

    # Toggle review like
    path('reviews/like/<int:review_id>', views.ToggleReviewLikeView.as_view()),

    # Get the list of the reviews the current user liked
    path('reviews/likes', views.ListUserLikedReviewsView.as_view()),

    # Get the list of the reviews the current user commented
    # path('reviews/comments', views.LustUserCommentedReviewsView.as_view()),

]
