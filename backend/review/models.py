from django.db import models

from user.models import User
from restaurant.models import Restaurant

RATING_CHOICES = [(1, "*"), (2, "**"), (3, "***"), (4, "****"), (5, "*****")]


class Review(models.Model):
    text = models.TextField(blank=False, verbose_name="Text")
    rating = models.IntegerField(choices=RATING_CHOICES, default=0, blank=False, verbose_name="Rating")
    likes_count = models.IntegerField(default=0, blank=False, verbose_name="Number of Likes")
    restaurant = models.ForeignKey(to=Restaurant, related_name='reviews', on_delete=models.CASCADE)
    user = models.ForeignKey(to=User, related_name='reviews', on_delete=models.CASCADE,
                             verbose_name="Author")
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"Review ID #{self.id} on '{self.restaurant}' by '{self.user}'"
