from rest_framework import status
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from review.models import Review
from restaurant.models import Restaurant
from user.models import User
from reviewlike.models import ReviewLike

from review.serializers import ReviewSerializer, ReviewAndRestaurantSerializer, ReviewAndUserSerializer
from review.permissions import IsOwnerOrAdminOrReadOnly


class ListRestaurantReviewsView(ListAPIView):
    """
    get:
    List all reviews

    Lists all reviews of all restaurants
    """
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer

    def list(self, request, *args, **kwargs):
        reviews = self.get_queryset()

        serializer = self.get_serializer(reviews, many=True)
        return Response(serializer.data)


class CreateRestaurantReviewView(CreateAPIView):
    """
    post:
    Create review for restaurant

    Create a review for a restaurant
    """
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    restaurant = None
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        restaurant_id = self.kwargs.get('restaurant_id')
        self.restaurant = get_object_or_404(Restaurant, id=restaurant_id)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user, restaurant=self.restaurant)


class ListRestaurantReviewView(ListAPIView):
    """
    get:
    List reviews by restaurant

    Lists all reviews of a restaurants
    """
    queryset = Review.objects.all()
    serializer_class = ReviewAndUserSerializer

    def list(self, request, *args, **kwargs):
        restaurant_id = self.kwargs.get('restaurant_id')
        restaurant = get_object_or_404(Restaurant, id=restaurant_id)
        reviews = self.get_queryset().filter(restaurant=restaurant)

        serializer = self.get_serializer(reviews, many=True)
        return Response(serializer.data)


class ListUserReviewView(ListAPIView):
    """
    get:
    List reviews by user

    Lists all reviews written by a user
    """
    queryset = Review.objects.all()
    serializer_class = ReviewAndRestaurantSerializer

    def list(self, request, *args, **kwargs):
        user_id = self.kwargs.get('user_id')
        user = get_object_or_404(User, id=user_id)
        reviews = self.get_queryset().filter(user=user)

        serializer = self.get_serializer(reviews, many=True)
        return Response(serializer.data)


class RetrieveUpdateDeleteReviewView(RetrieveUpdateDestroyAPIView):
    """
    get:
    Get a single review

    Returns a single review


    patch:
    Update a single review

    Partially updates a single review


    delete:
    Delete a single review

    Deletes a single review
    """
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = [IsOwnerOrAdminOrReadOnly]


class ToggleReviewLikeView(CreateAPIView):
    """
    post:
    Toggle like for review

    Likes or unlikes review
    """
    queryset = ReviewLike.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        user = request.user
        review = get_object_or_404(Review, id=kwargs.get('review_id'))

        serializer = self.get_serializer(review)

        existing_like = self.queryset.filter(user=user, review=review)

        if existing_like:
            # Remove like
            existing_like.delete()
            review.likes_count = review.likes_count - 1
            review.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        elif user == review.user:
            # Do nothing; cannot like own posts
            return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)

        else:
            # Add like
            like = ReviewLike(
                user=user,
                review=review,
            )
            like.save()
            review.likes_count = review.likes_count + 1
            review.save()
            return Response(serializer.data, status=status.HTTP_200_OK)


class ListUserLikedReviewsView(ListAPIView):
    """
    get:
    List liked reviews

    List all reviews liked by current user
    """
    queryset = Review.objects.all()
    serializer_class = ReviewAndRestaurantSerializer
    permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        user = get_object_or_404(User, id=self.request.user.id)
        reviews = self.get_queryset().filter(review_likes__user=user)

        serializer = self.get_serializer(reviews, many=True)
        return Response(serializer.data)
