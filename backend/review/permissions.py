from rest_framework import permissions
from rest_framework.permissions import BasePermission


class IsAuthenticatedOrPublic(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        elif request.user.is_registered:
            return True

        else:
            return False


class IsOwnerOrAdminOrReadOnly(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        elif request.user == obj.user:
            return True
        elif request.user.is_staff:
            return True
        else:
            return False
