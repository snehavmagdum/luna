from django.contrib import admin
from .models import Review


class ReviewAdmin(admin.ModelAdmin):
    list_display = ['id', 'restaurant', 'user', 'rating', 'created_date']


admin.site.register(Review, ReviewAdmin)
