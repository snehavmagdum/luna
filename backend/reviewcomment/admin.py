from django.contrib import admin
from .models import ReviewComment


class ReviewCommentAdmin(admin.ModelAdmin):
    list_display = ['id', 'review', 'user', 'text', 'created_date']


admin.site.register(ReviewComment, ReviewCommentAdmin)
