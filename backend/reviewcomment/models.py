from django.db import models

from user.models import User
from review.models import Review


class ReviewComment(models.Model):
    text = models.TextField(blank=False, verbose_name="Text")
    review = models.ForeignKey(to=Review, related_name='review_comments', on_delete=models.CASCADE)
    user = models.ForeignKey(to=User, related_name='review_comments', on_delete=models.CASCADE,
                             verbose_name="Author")
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"Comment ID #{self.id} on '{self.review}' by '{self.user}'"
