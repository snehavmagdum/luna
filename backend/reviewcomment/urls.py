from django.urls import path
from reviewcomment import views

urlpatterns = [

    # List all review-comments of a user
    path('comments/user/<int:user_id>', views.ListUserReviewCommentsView.as_view()),

    # Create a comment on a review
    path('reviews/<int:review_id>/comment', views.CreateReviewCommentView.as_view()),

    # Delete a comment
    path('comments/delete/<int:pk>', views.DeleteReviewCommentView.as_view()),

]
