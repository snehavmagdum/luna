from rest_framework import serializers

from user.serializers import UserSerializer, UserShortSerializer
from review.serializers import ReviewSerializer

from reviewcomment.models import ReviewComment


class ReviewCommentSerializer(serializers.ModelSerializer):

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        request = self.context.get('request')
        representation['user'] = UserSerializer(instance.user, context={'request': request}).data
        representation['review'] = ReviewSerializer(instance.review, context={'request': request}).data
        return representation

    class Meta:
        model = ReviewComment
        fields = [
            "id",
            "text",
            "review",
            "user",
            "created_date",
        ]
        extra_kwargs = {
            'id': {'read_only': True},
            'user': {'read_only': True},
            'review': {'read_only': True},
            'text': {'required': True},
        }


class ReviewCommentShortSerializer(serializers.ModelSerializer):

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        request = self.context.get('request')
        representation['user'] = UserShortSerializer(instance.user, context={'request': request}).data
        return representation

    class Meta:
        model = ReviewComment
        fields = [
            "id",
            "text",
            "user",
            "created_date",
        ]
        extra_kwargs = {
            'id': {'read_only': True},
            'user': {'read_only': True},
            'text': {'required': True},
        }
