from rest_framework import status
from rest_framework.generics import CreateAPIView, ListAPIView, get_object_or_404, DestroyAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from review.models import Review
from user.models import User
from reviewcomment.models import ReviewComment

from reviewcomment.serializers import ReviewCommentSerializer

from reviewcomment.permissions import IsOwnerOrAdminOrReadOnly

from review.serializers import ReviewSerializer


class ListUserReviewCommentsView(ListAPIView):
    """
    get:
    List all comments of a user

    List all comments that a user created
    """
    queryset = ReviewComment.objects.all()
    serializer_class = ReviewCommentSerializer
    permission_classes = [IsOwnerOrAdminOrReadOnly]

    def list(self, request, *args, **kwargs):
        user_id = self.kwargs.get('user_id')
        user = get_object_or_404(User, id=user_id)
        comments = self.get_queryset().filter(user=user)

        serializer = self.get_serializer(comments, many=True)
        return Response(serializer.data)


class CreateReviewCommentView(CreateAPIView):
    """
    post:
    Create comment for a review

    Create a comment for a review
    """
    queryset = ReviewComment.objects.all()
    review = None
    serializer_class = ReviewCommentSerializer
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        review_id = self.kwargs.get('review_id')
        self.review = get_object_or_404(Review, id=review_id)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        review_serializer = ReviewSerializer(self.review, context={'request': request})

        headers = self.get_success_headers(serializer.data)
        return Response(review_serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user, review=self.review)


class DeleteReviewCommentView(DestroyAPIView):
    """
    delete:
    Delete a comment

    Deletes a comment
    """
    queryset = ReviewComment.objects.all()
    serializer_class = ReviewCommentSerializer
    permission_classes = [IsOwnerOrAdminOrReadOnly]

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)
