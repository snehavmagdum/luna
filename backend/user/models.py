import datetime
import hashlib

from django.contrib.auth.models import AbstractUser
from django.db import models


def user_image_path(instance, filename):
    email_hash = hashlib.md5(instance.email.encode()).hexdigest()
    ext = filename.split('.')[-1]
    return f"user/{email_hash}/avatar.{ext}"


class User(AbstractUser):
    # Field used for authentication
    USERNAME_FIELD = "email"
    # Additional fields required when using createsuperuser (USERNAME_FIELD and passwords are always required)
    REQUIRED_FIELDS = ["username", "first_name", "last_name"]
    email = models.EmailField(unique=True)
    username = models.CharField(max_length=255, null=True, blank=True, verbose_name="User name", unique=False)
    first_name = models.CharField(
        max_length=255, null=False, blank=True, verbose_name="First name", unique=False
    )
    last_name = models.CharField(
        max_length=255, null=False, blank=True, verbose_name="Last name", unique=False
    )
    avatar = models.ImageField(
        upload_to=user_image_path, verbose_name="Avatar", blank=True
    )

    location = models.CharField(
        max_length=255, null=False, blank=True, verbose_name="Location", unique=False
    )
    phone_number = models.CharField(
        max_length=255, null=False, blank=True, verbose_name="Phone", unique=False
    )
    description = models.TextField(blank=True, verbose_name="Description")
    things_liked = models.TextField(blank=True, verbose_name="Things liked")
    registered_date = models.DateTimeField(default=datetime.datetime.now)
    last_login_date = models.DateTimeField(auto_now=True)
    is_registered = models.BooleanField(default=False, verbose_name="Is fully registered")

    def __str__(self):
        return f"{self.username} ({self.email})"
