from rest_framework import serializers

from user.models import User

from review.models import Review


class UserSerializer(serializers.ModelSerializer):
    logged_in_user_is_self = serializers.SerializerMethodField()
    num_reviews = serializers.SerializerMethodField()
    num_comments = serializers.SerializerMethodField()

    def get_logged_in_user_is_self(self, user):
        request = self.context.get('request')
        if request and request.user.is_authenticated:
            if request.user == user:
                return True
        return False

    def get_num_reviews(self, instance):
        return Review.objects.filter(user=instance).count()

    def get_num_comments(self, instance):
        return 5
        # return ReviewComment.objects.filter(user=instance).count()

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        request = self.context.get('request')
        if representation['avatar']:
            representation['avatar'] = request.build_absolute_uri(representation['avatar'])
        return representation

    class Meta:
        model = User
        fields = [
            "id",
            "username",
            "email",
            "first_name",
            "last_name",
            "avatar",
            "location",
            "phone_number",
            "description",
            "things_liked",
            "is_registered",
            "registered_date",
            "logged_in_user_is_self",
            "num_reviews",
            "num_comments",
        ]
        extra_kwargs = {
            'id': {'read_only': True},
        }


class UserShortSerializer(serializers.ModelSerializer):

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        request = self.context.get('request')
        if representation['avatar']:
            representation['avatar'] = request.build_absolute_uri(representation['avatar'])
        return representation

    class Meta:
        model = User
        fields = [
            "id",
            "username",
            "email",
            "first_name",
            "last_name",
            "avatar",
        ]
        extra_kwargs = {
            'id': {'read_only': True},
        }
