from rest_framework import status
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView, get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from user.models import User
from user.serializers import UserSerializer
from user.permissions import IsOwnerOrAdminOrReadOnly, IsAdminOrReadOnly


class ListCreateUserView(ListCreateAPIView):
    """
    get:
    List users

    List all users

    post:
    Create user

    Create a new user (for now allowed to authenticated users)
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAdminOrReadOnly]

    def list(self, request, *args, **kwargs):
        registered_users = self.get_queryset().filter(is_registered=True)

        serializer = self.get_serializer(registered_users, many=True)
        return Response(serializer.data)


class RetrieveUpdateDestroyUserView(RetrieveUpdateDestroyAPIView):
    """
    get:
    Retrieve user

    Retrieve a user by ID (restricted to Admins)

    put:
    Update user

    Update a user (restricted to Admins)

    patch:
    Partially update user

    Partially update a user (restricted to Admins)

    delete:
    Delete user

    Delete user (restricted to the user who is logged-in)
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsOwnerOrAdminOrReadOnly]


class RetrieveUpdateDestroyMeView(RetrieveUpdateDestroyAPIView):
    """
    get:
    Retrieve own user

    Retrieve own user

    put:
    Update own user

    Update own user

    patch:
    Partially update own user

    Partially update own user

    delete:
    Delete own user

    Delete own user
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]

    def retrieve(self, request, *args, **kwargs):  # Overwrite of the RetrieveModelMixin
        instance = request.user  # Ensure current user is retrieved
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):  # Overwrite of the UpdateModelMixin
        partial = kwargs.pop('partial', False)
        instance = request.user  # Ensure the current user is updated
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = request.user
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)


class ListSingleUserView(ListAPIView):
    """
    get:
    List one user

    List one user

    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAdminOrReadOnly]

    def list(self, request, *args, **kwargs):
        user_id = self.kwargs.get("user_id")
        user = get_object_or_404(User, id=user_id)
        serializer = self.get_serializer(user, many=False)
        return Response(serializer.data)
