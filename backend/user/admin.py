from django.contrib import admin
from .models import User
from django.contrib.auth.admin import UserAdmin


@admin.register(User)
class UserAdmin(UserAdmin):
    readonly_fields = ('last_login', 'date_joined',)
    # fields shown when creating a new instance
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'username', 'first_name', 'last_name', 'password1', 'password2')}
         ),
    )
    # fields when reading / updating an instance
    fieldsets = (
        (None, {'fields': ('email', 'username', 'password')}),
        ('Personal info',
         {'fields': ('first_name', 'last_name', 'avatar', 'location', 'phone_number', 'description', 'things_liked')}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', 'user_permissions')}),
        ('Groups', {'fields': ('groups',)}),
        ('Meta', {'fields': ('last_login', 'date_joined', 'is_registered')}),

    )
    # fields which are shown when looking at a list of instances
    list_display = ('id', 'username', 'email', 'first_name', 'last_name', 'is_registered')
    ordering = ('email',)
    list_filter = ['is_staff']

    filter_horizontal = 'groups', 'user_permissions'
