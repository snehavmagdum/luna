from django.urls import path
from user import views

urlpatterns = [

    # List/create users
    path('users/', views.ListCreateUserView.as_view()),

    # List one user
    path('users/<int:user_id>', views.ListSingleUserView.as_view()),

    # Retrieve/update current user
    path('me/', views.RetrieveUpdateDestroyMeView.as_view()),

]
