"""
URL configuration for Motion project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt import views as jwt_views
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

schema_view = get_schema_view(
    openapi.Info(
        title="Luna API",
        default_version="v1",
        description="Yet another YELP clone",
        contact=openapi.Contact(email="alexander@muedespacher.ch"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,  # Set to False restrict access to protected endpoints
    permission_classes=[permissions.AllowAny],  # Permissions for docs access
)

urlpatterns = [
    # Admin backoffice
    path("backend/admin/", admin.site.urls),

    # User registration
    path("backend/api/registration/", include("registration.urls")),

    # Users / me
    path("backend/api/", include("user.urls")),

    # Restaurants
    path("backend/api/", include("restaurant.urls")),

    # Reviews
    path("backend/api/", include("review.urls")),

    # Search
    path("backend/api/", include("search.urls")),

    # Review comments
    path("backend/api/", include("reviewcomment.urls")),


    # auth
    path(
        "backend/api/auth/token/",
        jwt_views.TokenObtainPairView.as_view(),
        name="token_obtain_pair",
    ),
    path(
        "backend/api/auth/token/refresh/",
        jwt_views.TokenRefreshView.as_view(),
        name="token_refresh",
    ),
    path(
        "backend/api/auth/token/verify/",
        jwt_views.TokenVerifyView.as_view(),
        name="token_refresh",
    ),
    # API docs
    path(
        "backend/api/docs/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
]

if settings.DEBUG:  # we only want to add these urls in debug mode
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
