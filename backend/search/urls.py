from django.urls import path
from search import views

urlpatterns = [

    # Search restaurants
    path('search/restaurants/', views.SearchRestaurantsView.as_view()),

    # Search restaurants
    path('search/reviews/', views.SearchReviewsView.as_view()),


]
