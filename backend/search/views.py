from rest_framework.generics import ListAPIView
from rest_framework.response import Response

from restaurant.models import Restaurant
from restaurant.serializers import RestaurantSerializer

from restaurant.models import CATEGORY_CHOICES

from review.models import Review
from review.serializers import ReviewSerializer


def is_valid_category(term, category_choices):
    return any(key for key, value in category_choices if key == term)


class SearchRestaurantsView(ListAPIView):
    """
    get:
    Search restaurants

    List all restaurants that match a search term and/or a category

    Examples
    ?category=swiss
    ?search_term=some_word
    ?category=swiss&search_term=some_word
    """
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer

    def list(self, request, *args, **kwargs):
        category = self.request.query_params.get('category')
        search_term = self.request.query_params.get('search_term')

        restaurants = []

        if search_term and is_valid_category(category, CATEGORY_CHOICES):
            restaurants = self.get_queryset().filter(category=category, name__icontains=search_term)
        elif search_term:
            restaurants = self.get_queryset().filter(name__icontains=search_term)
        elif is_valid_category(category, CATEGORY_CHOICES):
            restaurants = self.get_queryset().filter(category=category)
        else:
            pass

        serializer = self.get_serializer(restaurants, many=True)
        return Response(serializer.data)


class SearchReviewsView(ListAPIView):
    """
    get:
    Search reviews

    List all reviews that match a search term

    Example
    ?search_term=some_word
    """
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer

    def list(self, request, *args, **kwargs):
        search_term = self.request.query_params.get('search_term')

        reviews = []

        if search_term:
            reviews = self.get_queryset().filter(text__icontains=search_term)
        else:
            pass

        serializer = self.get_serializer(reviews, many=True)
        return Response(serializer.data)
