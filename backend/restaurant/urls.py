from django.urls import path
from restaurant import views

urlpatterns = [

    # List or create restaurants
    path('restaurants/', views.ListCreateRestaurantsView.as_view()),

    # List all restaurants of a category
    path('restaurants/category/<str:category>', views.ListRestaurantsPerCategoryView.as_view()),

    # List all restaurants of a user
    path('restaurants/user/<int:pk>', views.ListRestaurantsPerOwnerView.as_view()),

    # Retrieve, patch, delete one restaurant
    path('restaurants/<int:pk>', views.RetrieveUpdateDeleteRestaurantView.as_view()),

    # List all categories
    path('categories/', views.ListCategoriesView.as_view()),

    # List all countries
    path('countries/', views.ListCountriesView.as_view()),

    # List top restaurants
    path('home/', views.ListTopRestaurantsView.as_view()),

]
