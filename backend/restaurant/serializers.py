from django.db.models import Avg
from rest_framework import serializers

from restaurant.models import Restaurant
from review.models import Review

from user.serializers import UserSerializer


class RestaurantSerializer(serializers.ModelSerializer):
    num_reviews = serializers.SerializerMethodField()
    avg_rating = serializers.SerializerMethodField()
    country_name = serializers.SerializerMethodField()
    category_name = serializers.SerializerMethodField()

    def get_num_reviews(self, instance):
        return Review.objects.filter(restaurant=instance).count()

    def get_avg_rating(self, instance):
        average = Review.objects.filter(restaurant=instance).aggregate(Avg('rating'))['rating__avg']
        return round(average, 2) if average is not None else 0.00

    def get_country_name(self, instance):
        return instance.get_country_display()

    def get_category_name(self, instance):
        return instance.get_category_display()

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        request = self.context.get('request')
        representation['owner'] = UserSerializer(instance.owner, context={'request': request}).data
        if representation['image']:
            representation['image'] = request.build_absolute_uri(representation['image'])
        return representation

    class Meta:
        model = Restaurant
        fields = [
            "id",
            "name",
            "category",
            "category_name",
            "country",
            "country_name",
            "street",
            "city",
            "zip",
            "website",
            "phone",
            "email",
            "opening_hours",
            "price_level",
            "image",
            "owner",
            "num_reviews",
            "avg_rating",
        ]
        extra_kwargs = {
            'id': {'read_only': True},
            'owner': {'read_only': True},
            'country_name': {'read_only': True},
            'num_reviews': {'read_only': True},
            'avg_rating': {'read_only': True},
            'name': {'required': True},
            'category': {'required': True},
            'country': {'required': True},
            'street': {'required': True},
            'city': {'required': True},
            'zip': {'required': True},
            'website': {'required': False},
            'phone': {'required': False},
            'opening_hours': {'required': True},
            'price_level': {'required': True},
            'image': {'required': False},
        }


class RestaurantShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Restaurant
        fields = [
            "id",
            "name",
        ]
        extra_kwargs = {
            'id': {'read_only': True},
            'name': {'read_only': True},
        }
