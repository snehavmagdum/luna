import hashlib

from django.db import models
from django_countries.fields import CountryField

from user.models import User

CATEGORY_CHOICES = [
    ("traditional-german", "Traditional German"),
    ("berlin-street-food", "Berlin Street Food"),
    ("french-brasserie", "French Brasserie"),
    ("mediterranean", "Mediterranean"),
    ("turkish-kebab", "Turkish Kebab"),
    ("polish", "Polish"),
    ("czech", "Czech"),
    ("hungarian", "Hungarian"),
    ("swiss", "Swiss"),
    ("belgian", "Belgian"),
    ("dutch", "Dutch"),
    ("scandinavian", "Scandinavian"),
    ("diet-restricted", "Diet Restricted (Vegan, Vegetarian, Gluten-Free etc.)"),
    ("tapas-bar", "Tapas Bar"),
    ("wine-bar", "Wine Bar"),
    ("brewery-pub", "Brewery Pub"),
    ("coffee-shop", "Coffee Shop"),
    ("tea-house", "Tea House"),
    ("bakery-pastry", "Bakery & Pastry"),
    ("ice-cream-parlor", "Ice Cream Parlor"),
    ("chocolate-shop", "Chocolate Shop"),
    ("fine-dining", "Fine Dining"),
    ("thai", "Thai"),
    ("japanese", "Japanese"),
    ("indian", "Indian"),
    ("vietnamese", "Vietnamese"),
    ("chinese", "Chinese"),
    ("other", "Other restaurant"),
]

PRICE_LEVEL_CHOICES = [
    (1, "$"),
    (2, "$$"),
    (3, "$$$"),
    (4, "$$$$"),
]


def restaurant_image_path(instance, filename):
    restaurant_hash = hashlib.md5(f"{instance.name}_{instance.created_date}".encode()).hexdigest()
    ext = filename.split('.')[-1]
    return f"restaurant/{restaurant_hash}/image.{ext}"


class Restaurant(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False, verbose_name="Name")
    category = models.CharField(max_length=255, choices=CATEGORY_CHOICES, verbose_name="Category", default="other")
    country = CountryField(verbose_name="Country", default="CH")
    street = models.CharField(max_length=255, null=False, blank=False, verbose_name="Street")
    city = models.CharField(max_length=255, null=False, blank=False, verbose_name="City")
    zip = models.CharField(max_length=255, null=False, blank=False, verbose_name="ZIP code")
    website = models.URLField(max_length=255, null=False, blank=True, verbose_name="Website")
    phone = models.CharField(max_length=255, null=False, blank=True, verbose_name="Phone number")
    email = models.EmailField(max_length=255, null=False, blank=True, verbose_name="eMail")
    opening_hours = models.CharField(max_length=255, null=False, blank=False, verbose_name="Opening hours")
    price_level = models.IntegerField(choices=PRICE_LEVEL_CHOICES, default=1, verbose_name="Price level")
    image = models.ImageField(upload_to=restaurant_image_path, verbose_name="Image", blank=True)
    owner = models.ForeignKey(to=User, related_name='restaurants', on_delete=models.CASCADE,
                              verbose_name="Owner")
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"Restaurant ID #{self.id}: {self.name}"
