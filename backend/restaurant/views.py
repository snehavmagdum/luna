from django.db.models import Avg, FloatField
from django.db.models.functions import Coalesce
from rest_framework import status
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView, get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from django_countries import countries

from restaurant.models import Restaurant
from restaurant.serializers import RestaurantSerializer

from restaurant.permissions import IsAuthenticatedOrPublic

from restaurant.permissions import IsOwnerOrAdminOrReadOnly

from user.models import User

from restaurant.models import CATEGORY_CHOICES


def is_valid_category(term, category_choices):
    return any(key for key, value in category_choices if key == term)


class ListCreateRestaurantsView(ListCreateAPIView):
    """
    get:
    List restaurants

    List all restaurants

    post:
    Create restaurant

    Create a new restaurant (for now allowed to authenticated users)
    """
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer
    permission_classes = [IsAuthenticatedOrPublic]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class RetrieveUpdateDeleteRestaurantView(RetrieveUpdateDestroyAPIView):
    """
    get:
    Retrieve restaurant

    Retrieve a restaurant by ID

    put:
    Update restaurant

    Update a restaurant

    patch:
    Partially update restaurant

    Partially update a restaurant

    delete:
    Delete restaurant

    Delete restaurant
    """
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer
    permission_classes = [IsOwnerOrAdminOrReadOnly]


class ListRestaurantsPerCategoryView(ListAPIView):
    """
    get:
    List all restaurants per category

    List all restaurants of a given category
    """
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer

    def list(self, request, *args, **kwargs):
        category = self.kwargs.get('category')

        if is_valid_category(category, CATEGORY_CHOICES):
            restaurants = self.get_queryset().filter(category=category)
            serializer = self.get_serializer(restaurants, many=True)
            return Response(serializer.data)
        else:
            return Response({"error": "Category not found"}, status=status.HTTP_404_NOT_FOUND)


class ListRestaurantsPerOwnerView(ListAPIView):
    """
    get:
    List all restaurants per user

    List all restaurants of a given user
    """
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer

    def list(self, request, *args, **kwargs):
        user_id = self.kwargs.get('pk')
        user = get_object_or_404(User, id=user_id)
        restaurants = self.get_queryset().filter(owner=user)
        serializer = self.get_serializer(restaurants, many=True)
        return Response(serializer.data)


class ListCategoriesView(APIView):
    """
    get:
    List all categories

    List all restaurant categories
    """
    def get(self, request, *args, **kwargs):
        dict_list = [{"key": key, "name": name} for key, name in CATEGORY_CHOICES]
        return Response(dict_list, status=status.HTTP_200_OK)


class ListCountriesView(APIView):
    """
    get:
    List all countries

    List all countries
    """

    def get(self, request, *args, **kwargs):
        dict_list = [{"key": code, "name": name} for code, name in countries]
        return Response(dict_list, status=status.HTTP_200_OK)


class ListTopRestaurantsView(ListAPIView):
    """
    get:
    List top restaurants

    List top restaurants based on the average rating
    Pass on a limit=x query param; defaults to 4
    """
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer

    def list(self, request, *args, **kwargs):
        limit = int(request.query_params.get("limit", 4))  # set to 4 if not defined in query params
        top_restaurants = Restaurant.objects.annotate(
            avg_rating=Coalesce(Avg('reviews__rating'), 0, output_field=FloatField())
        ).order_by('-avg_rating')[:limit]

        serializer = self.get_serializer(top_restaurants, many=True)
        return Response(serializer.data)
