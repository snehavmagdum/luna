import random
from datetime import datetime, timedelta
from django.db import IntegrityError

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

from registration.models import RegistrationToken
from user.models import User

from email_service.sendgrid import send_email


class RegisterUserView(APIView):
    """
    post:
    Register as new user

    Registering a new user with an email and requesting a validation code

    """
    responses = {
        status.HTTP_201_CREATED: openapi.Response(description="User initially registered; token generated"),
        status.HTTP_400_BAD_REQUEST: openapi.Response(
            description="User is already fully registered."),
        status.HTTP_500_INTERNAL_SERVER_ERROR: openapi.Response(description="Token generated, but email sending failed")
    }

    @swagger_auto_schema(responses=responses)
    def post(self, request):
        email = request.data.get("email")

        if not email:
            return Response({"message": "No email provided"}, status=status.HTTP_400_BAD_REQUEST)

        existing_user = User.objects.filter(email=email, is_registered=True)

        if existing_user.exists():
            return Response({"message": "Could not create account"}, status=status.HTTP_400_BAD_REQUEST)

        user, created = User.objects.get_or_create(
            email=email,
            defaults={'is_registered': False},
        )

        token = RegistrationToken(
            user=user,
            expiry_date=datetime.now() + timedelta(hours=2),
            code=random.randint(100000, 999999)
        )

        token.save()

        # Send eMail
        email = send_email(
            user=user,
            event="registration",
            template_data={"code": token.code}
        )

        if email:
            return Response({"message": "Token generated. Check inbox"}, status=status.HTTP_201_CREATED)

        else:
            return Response({"message": "Token generated, but could not send eMail"},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ValidateRegisterUserView(APIView):
    """
    post:
    Validate registration of a new user

    Validation registration of a new user with code and other inputs
    """

    responses = {
        status.HTTP_200_OK: openapi.Response(description="User successfully registered"),
        status.HTTP_400_BAD_REQUEST: openapi.Response(
            description="Bad Request. Possible reasons could include missing required fields, non-matching passwords, or an existing username."),
        status.HTTP_400_BAD_REQUEST: openapi.Response(description="User or token not found, or the token is invalid"),
    }

    @swagger_auto_schema(responses=responses)
    def post(self, request):
        try:
            user = User.objects.get(email=request.data.get('email'))
        except User.DoesNotExist:
            return Response({"message": "User not found"}, status=status.HTTP_404_NOT_FOUND)

        token = RegistrationToken.objects.filter(
            user=user,
            expiry_date__gte=datetime.now(),
            used_date__isnull=True,
        ).order_by('-created_date').first()

        if token is not None and request.data.get('code') == token.code:
            # Token is valid

            required_fields = ['username', 'password', 'password_repeat', 'first_name', 'last_name']

            missing_fields = [field for field in required_fields if not request.data.get(field)]

            if missing_fields:
                return Response({"message": f"Missing required fields: {', '.join(missing_fields)}"},
                                status=status.HTTP_400_BAD_REQUEST)

            if request.data.get('password') != request.data.get('password_repeat'):
                return Response({"message": "Passwords must match"},
                                status=status.HTTP_400_BAD_REQUEST)

            try:
                user.username = request.data.get('username')
                user.set_password(request.data.get(
                    'password'))
                user.first_name = request.data.get('first_name')
                user.last_name = request.data.get('last_name')
                user.is_registered = True
                user.save()
            except IntegrityError:
                return Response({"message": "This username already exists"}, status=status.HTTP_400_BAD_REQUEST)

            token.used_date = datetime.now()
            token.save()

            return Response({"message": "User successfully registered"}, status=status.HTTP_200_OK)

        return Response({"message": "Token not found or invalid"}, status=status.HTTP_400_BAD_REQUEST)
