from django.urls import path
from registration import views

urlpatterns = [

    # RegisterUser
    path('', views.RegisterUserView.as_view()),

    # Validate registration
    path('validate/', views.ValidateRegisterUserView.as_view()),


]
