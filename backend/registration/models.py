from django.db import models

from user.models import User


class RegistrationToken(models.Model):
    user = models.ForeignKey(to=User, related_name='requested_by', on_delete=models.CASCADE,
                             verbose_name="User (Requester)")
    expiry_date = models.DateTimeField(blank=False, verbose_name="Expiry date")
    used_date = models.DateTimeField(blank=True, null=True, verbose_name="Used date")
    code = models.CharField(max_length=10, null=False, blank=False, verbose_name="Registration token")
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Token {self.code} for {self.user}"
