from django.contrib import admin
from .models import RegistrationToken


class RegistrationTokenAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'code', 'created_date', 'expiry_date', 'used_date']


admin.site.register(RegistrationToken, RegistrationTokenAdmin)
