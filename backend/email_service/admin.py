import json

from django.contrib import admin
from .models import EmailNotification


class EmailNotificationAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'event', 'created_date']

    fields = ['user', 'event', 'api_request', 'api_response']
    readonly_fields = ['user', 'event', 'api_request', 'api_response']

    def api_request(self, obj):
        return json.dumps(json.loads(obj.request), indent=4)

    def api_response(self, obj):
        return json.dumps(json.loads(obj.response), indent=4)


admin.site.register(EmailNotification, EmailNotificationAdmin)
