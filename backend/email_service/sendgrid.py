import json
import os
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from sendgrid.helpers.mail import From

from email_service.models import EmailNotification


def send_email(user, event, template_data):
    template_ids = {
        "registration": "d-80dc12bccdbb44d5a9b6e49d6bbfb38e",
    }

    template_id = template_ids.get(event)

    if template_id is None:
        return False

    message = Mail(
        from_email=From(os.environ.get('SENDGRID_FROM_ADDRESS'), "Luna Team"),
        to_emails=user.email,
    )

    message.dynamic_template_data = template_data
    message.template_id = template_id

    try:
        sendgrid_client = SendGridAPIClient(os.environ.get('SENDGRID_API_KEY'))
        response = sendgrid_client.send(message)
    except Exception as e:
        response = {
            "status_code": 500,
            "body": e.message
        }

    request = {
        "template_id": template_id,
        "template_data": template_data,
    }

    log = EmailNotification(
        user=user,
        event=event,
        request=json.dumps(request),
        response=json.dumps(
            {
                "status_code": response.status_code,
                "body": response.body.decode('utf-8'),
            }
        ))

    log.save()

    if response.status_code == 202:
        return True
    else:
        return False
