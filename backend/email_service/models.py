from django.db import models

from user.models import User


class EmailNotification(models.Model):
    user = models.ForeignKey(to=User, related_name='sent_to', on_delete=models.CASCADE,
                             verbose_name="Recipient")
    event = models.CharField(max_length=50, null=False, blank=False, verbose_name="Event")
    request = models.JSONField(verbose_name="Request")
    response = models.JSONField(verbose_name="Response")
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Email (Event: {self.event}) sent to {self.user}"
