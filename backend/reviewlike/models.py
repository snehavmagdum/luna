from django.db import models
from user.models import User
from review.models import Review


class ReviewLike(models.Model):
    user = models.ForeignKey(User, related_name='review_likes', on_delete=models.CASCADE)
    review = models.ForeignKey(Review, related_name='review_likes', on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Like by '{self.liker}' on '{self.review}'"
