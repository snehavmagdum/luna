export const extractDate = (inputDate) => {
  const dateObj = new Date(inputDate);
  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  return {
    month: months[dateObj.getMonth()],
    monthNumber: String(dateObj.getMonth() + 1).padStart(2, "0"),
    date: String(dateObj.getDate()).padStart(2, "0"),
    year: dateObj.getFullYear(),
    yearTruncated: dateObj.getYear(),
    hour: dateObj.getHours(),
    minute: dateObj.getMinutes(),
  };
};
