// store.js
import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./slices/user";
import searchReducer from "./slices/search";
import restaurantReducer from "./slices/restaurant";
import profileReducer from "./slices/profile";
import constantsReducer from "./slices/constants.js"
import reviewReducer from "./slices/review.js"

const store = configureStore({
  reducer: {
    user: userReducer,
    search: searchReducer,
    restaurant: restaurantReducer,
    profile: profileReducer,
    constants: constantsReducer,
    review:reviewReducer,
  },
});

export default store;
