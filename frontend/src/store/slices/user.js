import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    accessToken: undefined,
    details: null,
    registration: {
        email: "",
        code: "",
        first_name: "",
        last_name: "",
        location: "",
        username: "",
        password: "",
        password_repeat: "",
    },
};

const userSlice = createSlice({
    name: "user",
    initialState,
    reducers: {
        login: (state, action) => {
            state.accessToken = action.payload;
        },
        logout: (state) => {
            state.accessToken = null;
            state.details = null;
        },
        loadUserDetails: (state, action) => {
            state.details = action.payload;
        },
        setRegistration: (state, action) => {
            state.registration = {...state.registration, ...action.payload};
        },
        resetRegistration: (state) => {
            state.registration = {
                email: "",
                code: "",
                first_name: "",
                last_name: "",
                location: "",
                username: "",
                password: "",
                password_repeat: "",
            };
        },
    },
});

export const {login, logout, loadUserDetails, setRegistration, resetRegistration} = userSlice.actions;
export default userSlice.reducer;
