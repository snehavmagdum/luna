import { createSlice } from '@reduxjs/toolkit';

// Create slice
const search = createSlice({
  name: 'search',
  initialState: {
    search: '',
  },
  reducers: {
    setSearch(state, action) {
      state.search = action.payload;
    },
  },
});

// Export actions
export const { setSearch } = search.actions;

export default search.reducer;
