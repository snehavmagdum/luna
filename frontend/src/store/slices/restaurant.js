import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    restaurants: [],
    searchedRestaurant: [],
    selectedRestaurant: null,
    selectedRestaurantReviews: [],
    searchField: '',
    categoryField: '',
};

const restaurantSlice = createSlice({
    name: "restaurant",
    initialState,
    reducers: {
        setRestaurants: (state, action) => {
            state.restaurants = action.payload;
        },
        setSelectedRestaurant: (state, action) => {
            state.selectedRestaurant = action.payload;
        },
        setSelectedRestaurantReviews: (state, action) => {
            state.selectedRestaurantReviews = action.payload;
        },
        addRestaurant: (state, action) => {
            state.restaurants.push(action.payload);
        },
        setSearchField: (state, action) => {
            state.searchField = action.payload;
        },
        setSearchedRestaurant: (state, action) => {
            state.searchedRestaurant = action.payload;
        },
        setCategoryField: (state, action) => {
            state.categoryField = action.payload;
        },
    },
});

export const {
    setRestaurants,
    setSelectedRestaurant,
    addRestaurant,
    setSearchField,
    setSearchedRestaurant,
    setSelectedRestaurantReviews,
    setCategoryField
} = restaurantSlice.actions;
export default restaurantSlice.reducer;
