import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    categories: [],
    countries: [],
};

const constantsSlice = createSlice({
    name: "constants",
    initialState,
    reducers: {
        setCategories: (state, action)=>{
            state.categories = action.payload;
        },
        setCountries: (state, action)=>{
            state.countries = action.payload;
        },
    },
});

export const {setCategories, setCountries} = constantsSlice.actions;
export default constantsSlice.reducer;
