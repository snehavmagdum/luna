import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    review: [],
    searchedReview:[],
    searchField:'',

};

const reviewSlice = createSlice({
    name: "review",
    initialState,
    reducers: {
        setReviews: (state, action)=>{
            state.review = action.payload;
        },
         setSearchedField: (state, action)=>{
            state.searchField = action.payload;
        },
         setSearchedReview: (state, action)=> {
            state.searchedReview=action.payload;
        },
    },
});

export const {setReviews,setSearchedField,setSearchedReview} = reviewSlice.actions;
export default reviewSlice.reducer;
