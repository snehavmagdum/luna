import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    profiles: [],
    selectedProfile: null,
    selectedProfileReviews: null,
    selectedProfileComments: null,
    selectedProfileRestaurants: null,
};

const profileSlice = createSlice({
    name: "profile",
    initialState,
    reducers: {
        setProfiles: (state, action) => {
            state.profiles = action.payload;
        },
        setSelectedProfile: (state, action) => {
            state.selectedProfile = action.payload;
        },
        setSelectedProfileReviews: (state, action) => {
            state.selectedProfileReviews = action.payload;
        },
        setSelectedProfileComments: (state, action) => {
            state.selectedProfileComments = action.payload;
        },
        setSelectedProfileRestaurants: (state, action) => {
            state.selectedProfileRestaurants = action.payload;
        },
        addProfile: (state, action) => {
            state.profiles.push(action.payload);
        },
    },
});

export const {
    setProfiles,
    setSelectedProfile,
    setSelectedProfileComments,
    setSelectedProfileRestaurants,
    setSelectedProfileReviews,
    addProfile
} =
    profileSlice.actions;
export default profileSlice.reducer;
