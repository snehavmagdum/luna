import styled from "styled-components";
import { SubheaderTitle } from "../styles/SubheaderTitle.js";
import Button from "./Button.jsx";

const RegistrationConfirmationContainer = styled.div`
  display: flex;
  padding-top: 4rem;
  width: 100%;
  height: 80vh;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  gap: 3rem;
  background: #F8F8F8;
`;

const ConfirmationMessage = styled.div`
  width: 25%;
  height: 40%;
  display: flex;
  flex-direction: column;
  justify-content: center;

  p {
    color: #4C4C4C;
    text-align: center;
    font-size: 20px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
`;

export default function RegistrationConfirmation({ handleRegistrationConfirmationComplete }) {
  const handleContinue = () => {
    handleRegistrationConfirmationComplete();
  };

  return (
    <RegistrationConfirmationContainer>
      <SubheaderTitle>Registration</SubheaderTitle>
      <ConfirmationMessage>
        <p>
          Thanks for your registration. Our hard working monkeys are preparing
          a digital message called E-Mail that will be sent to you soon. Since
          monkeys aren't good at writing, the message could end up in your junk
          folder. Our apologies for any inconvenience.
        </p>
      </ConfirmationMessage>
      <Button onClick={handleContinue}>Continue</Button>
    </RegistrationConfirmationContainer>
  );
}
