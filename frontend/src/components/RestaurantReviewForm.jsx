import styled from "styled-components";
import Button from "./Button";
import { useSelector, useDispatch } from "react-redux";
import { api } from "../axios";
import { useNavigate, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { ErrorMessage } from "../styles/ErrorMessage.js";
import { Rating } from "react-simple-star-rating";
import RestaurantBanner from "./RestaurantBanner";
import {
  addRestaurant,
  setSelectedRestaurant,
  setSelectedRestaurantReviews,
} from "../store/slices/restaurant.js";
import Loading from "./Loading.jsx";

const RestaurantReviewFormStyle = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  margin-bottom: 140px;
`;

const Form = styled.form`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  gap: 2rem;
  padding-top: 40px;
`;

const FormFields = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  gap: 2rem;
`;

const InputStyle = styled.textarea`
  flex-shrink: 0;
  border-radius: 0.1875rem;
  border: 1px solid #ebebeb;
  outline: none;
  width: 800px;
  height: 265px;
  background-color: white;
  padding: 10px;
  color: #333;
  font-size: 20px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;

  ::placeholder {
    color: #bbb7b7;
    font-size: 20px;
    font-style: normal;
    font-weight: 500;
    line-height: normal;
  }

  &:focus {
    border-color: #e47d31;
    box-shadow: 0 0 4px rgba(191, 79, 116, 0.5);
  }
`;

export default function RestaurantReviewForm() {
  const token = useSelector((state) => state.user.accessToken);
  const { restaurantId } = useParams();
  const restaurant = useSelector(
    (state) => state.restaurant.selectedRestaurant
  );

  const [rating, setRating] = useState(0);
  const [review, setReview] = useState("");

  const [error, setError] = useState("");
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [loading, setLoading] = useState(true);

  const fetchRestaurantData = async () => {
    try {
      // Fetch comments from the server or any other source
      const restaurantResponse = await api.get(`/restaurants/${restaurantId}`);
      dispatch(setSelectedRestaurant(restaurantResponse.data));

      const reviewsResponse = await api.get(
        `/reviews/restaurant/${restaurantId}`
      );
      dispatch(setSelectedRestaurantReviews(reviewsResponse.data));

      setLoading(false);
    } catch (error) {
      console.error("Error fetching restaurant and/or review data:", error);
    }
  };

  useEffect(() => {
    fetchRestaurantData();
  }, [restaurantId]);

  const handleSubmission = async (e) => {
    e.preventDefault();
    try {
      const res = await api.post(
        `/reviews/new/${restaurant.id}`,
        { text: review, rating: rating },
        {
          headers: { Authorization: "Bearer " + token },
        }
      );
      navigate(`/restaurants/${restaurant.id}`);
    } catch (e) {
      setError(e.response.data);
    }
  };

  const handleRating = (rate) => {
    setRating(rate);

    // other logic
  };

  if (loading) {
    return <Loading />;
  } else {
    return (
      <RestaurantReviewFormStyle>
        <RestaurantBanner data={restaurant} />
        <Form onSubmit={(e) => handleSubmission(e)}>
          <FormFields>
            <Rating
              onClick={handleRating}
              initialValue={1}
              allowFraction={false}
              iconsCount={5}
            />
            <p> Select your rating</p>
            <ErrorMessage>{error?.rating}</ErrorMessage>

            <InputStyle
              type="text"
              value={review}
              placeholder="Your review helps others learn about great local businesses. Please don't review this business if you received a freebie for writing this review, or if you're connected in any way to the owner or employees."
              onChange={(e) => setReview(e.target.value)}
            />
          </FormFields>
          <ErrorMessage>{error?.text}</ErrorMessage>
          <Button type="submit">Submit</Button>
        </Form>
      </RestaurantReviewFormStyle>
    );
  }
}
