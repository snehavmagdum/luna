import styled from "styled-components";

const EmptyContainer = styled.div`
  width: 100%;
  height: 40vh;
  display: flex;
  align-items: center;
  justify-items: center;
  color: #333;
  
  & p {
    width: 100%;
    height: fit-content;
    text-align: center;
  }
`

export default function Empty({message}) {

  return <EmptyContainer><p>{message}</p></EmptyContainer>;
}
