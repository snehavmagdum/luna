import styled from "styled-components";
import UserCardContainer from "./UserCardContainer.jsx";
import {setSearchedField} from "../store/slices/review.js";
import {useDispatch, useSelector} from "react-redux";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 10px;
`;

const InputSearch = styled.input.attrs({
    type: "text",
})`
  width: 100%;
  height: 50px;
  flex-shrink: 0;
  border-radius: 0.1875rem;
  border: 1px solid #ebebeb;
  background: #fff;
  padding-left: 1rem;
  outline: none;

  ::placeholder {
    color: #D8D8D8;
    font-size: 20px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
  }

  &:focus {
    border-color: #e47d31;
    box-shadow: 0 0 4px rgba(191, 79, 116, 0.5);
  }
`;

export default function UserSearch() {
    const searchField = useSelector(state => state.review.searchField)
    const dispatch = useDispatch();
    return <Container>
        <InputSearch placeholder='Search User..'  value={searchField}
                     onChange={(e) => dispatch(setSearchedField(e.target.value))}></InputSearch>
        <UserCardContainer></UserCardContainer>
    </Container>
}
