import React from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";
import HomeSearchImage from "../assets/HomeSearchImage.png";
import Button from "./Button";
import { setSearch } from "../store/slices/search";
import { useNavigate } from "react-router-dom";

const HomeSearchForm = styled.form`
  width: 100%;
  height: 351px;
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 4rem;
  background: url(${HomeSearchImage});
  background-blend-mode: multiply, normal;
  background-size: cover;
`;

const InputSearch = styled.input.attrs({
  type: "text",
})`
  width: 30rem;
  height: 3.25rem;
  flex-shrink: 0;
  border-radius: 0.1875rem;
  border: 1px solid #ebebeb;
  background: #fff;
  padding-left: 1rem;
  outline: none;
  color: #4C4C4C;
  font-size: 20px;
  font-style: normal;
  font-weight: 400;
  line-height: 20px;

  ::placeholder {
    color: #979797;
  }

  &:focus {
    border-color: #e47d31;
    box-shadow: 0 0 4px rgba(191, 79, 116, 0.5);
  }
`;

export default function HomeSearch() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  function handleSearch() {
    const searchValue = document.getElementById("inputSearch").value;
    dispatch(setSearch(searchValue));
    navigate(`/search/restaurants?s=${searchValue}`);
  }

  return (
    <HomeSearchForm onSubmit={handleSearch}>
      <InputSearch id="inputSearch" placeholder="Search for a restaurant..." />
      <Button type="submit">Search</Button>
    </HomeSearchForm>
  );
}
