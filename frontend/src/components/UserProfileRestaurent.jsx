import styled from "styled-components";
import StarRating from "./StarRating";

const SingleReviewWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 10px;
`;
const RestaurantTitle = styled.div`
  display: flex;
  justify-content: space-between;
  color: #303030;
  font-size: 1.25rem;
  font-style: normal;
  font-weight: 300;
  line-height: normal;
`;

const Description = styled.div`
  color: #303030;
  text-align: justify;
  font-size: 1rem;
  font-style: normal;
  font-weight: 300;
  line-height: normal;
`;

export default function UserProfileRestaurent({data}) {
    const {name, avg_rating, street, zip, city, website} = data
  // some other code here

  return (
    <SingleReviewWrapper>
      <RestaurantTitle>{name}</RestaurantTitle>
      <StarRating score={avg_rating} />
      <Description>
        <p>{street}<br/>{zip} {city}</p>
          <p>{website}</p>
      </Description>
    </SingleReviewWrapper>
  );
}
