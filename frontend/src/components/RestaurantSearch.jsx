import CardContainer from "./CardContainer.jsx";
import InlineSearchbarRestaurantSearch from "./InlineSearchbarRestaurantSearch.jsx";
import styled from "styled-components";
import {useEffect} from "react";
import {api} from "../axios/index.js";
import {useDispatch} from "react-redux";
import {setRestaurants} from "../store/slices/restaurant.js";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 10px;
`;

export default function RestaurantSearch() {
    const dispatch = useDispatch();

    useEffect(() => {
        fetchRestaurants();
    }, []);

    const fetchRestaurants = async () => {
        try {
            const response = await api.get("/restaurants/");
            dispatch(setRestaurants(response.data))
        } catch (e) {
            console.log(e);
        }
    };


    return <Container>
        <InlineSearchbarRestaurantSearch/>
        <CardContainer/>
    </Container>;
}
