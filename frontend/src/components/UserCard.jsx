import styled from "styled-components";
import UserProfileShort from "./UserProfileShort.jsx";
import {useNavigate} from "react-router-dom";

const UserCardContainer = styled.div`
  background-color: #fff;
  width: 272px;
  height: 192px;
  flex-shrink: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 0.5rem;
  border: 1px solid #EBEBEB;
  border-top: 5px solid #E47D31;
  cursor: pointer;
`;

const UserDetail = styled.div`
  display: flex;
  flex-flow: wrap;
  gap: 0.2rem;
  width: 255px;
  height: 100px;
  color: #4C4C4C;
  overflow: hidden;
  text-overflow: ellipsis;

  p {
    height: 80px;
    font-size: 14px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    overflow: hidden;
    white-space: pre-wrap;
    text-overflow: ellipsis;
  }

  a {
    color: #E47D31;
    text-decoration: none;

  }
`;


export default function UserCard({user}) {
    const navigate = useNavigate()

    return <UserCardContainer onClick={() => navigate(`/profile/${user?.id}`)}>
        <UserProfileShort userInfo={user}></UserProfileShort>
        <UserDetail><p>{user.description} </p><a
            href="#">read more</a></UserDetail>
    </UserCardContainer>;


}
