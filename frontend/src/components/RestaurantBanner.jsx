import styled from "styled-components";
import StarRating from "./StarRating.jsx";

const BannerContainer = styled.div`
  width: 100%;
  height: ${(props) => (`${props.height}px`)};
  background-size: cover;
  background-color: red;`

const RestaurantSummaryBar = styled.div`
  height: 200px;
  background-color: rgba(0, 0, 0, 0.5);
  display: flex;
  align-items: center;
  justify-content: center;
`

const RestaurantSummaryContainer = styled.div`
  width: 1180px;
  height: 100%;
  margin-top: 40px;
`

const RestaurantTitle = styled.h1`
  color: #FFF;
  font-size: 30px;
  font-style: normal;
  font-weight: 700;
  line-height: normal;
`

const RestaurantCategory = styled.h2`
  color: #FFF;
  font-size: 24px;
  font-style: normal;
  font-weight: 300;
  line-height: normal;
  margin-top: 12px`

const ReviewSummary = styled.div`
  display: flex;
  justify-content: start;
  align-items: center;
  gap: 30px;
  margin-top: 12px;
`

const ReviewCount = styled.p`
color: #FFF;
font-size: 20px;
font-style: normal;
font-weight: 300;
line-height: normal;
`


export default function RestaurantBanner({isLarge = false, data}) {
    const {name, image, category_name, avg_rating, num_reviews} = data

    return <BannerContainer height={isLarge ? 500 : 200} style={{backgroundImage: `url(${image})`}}>
        <RestaurantSummaryBar>
            <RestaurantSummaryContainer>
                <RestaurantTitle>{name}</RestaurantTitle>
                <RestaurantCategory>{category_name}</RestaurantCategory>
                <ReviewSummary>
                    <StarRating score={avg_rating}/>
                    <ReviewCount>{num_reviews} reviews</ReviewCount>
                </ReviewSummary>
            </RestaurantSummaryContainer>
        </RestaurantSummaryBar>
    </BannerContainer>;
}
