import styled from "styled-components";
import { extractDate } from "../../utils/extractDate";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  gap: 0.2rem;
  width: 100%;
  flex-shrink: 0;
  overflow: clip;
`;

const Comment = styled.div`
  color: #000;
  font-size: 20px;
  font-style: normal;
  font-weight: 300;
  line-height: normal;
  overflow: clip;
  padding-top: 7px;
`;

const NameAndDate = styled.div`
  display: flex;
  justify-content: space-between;
  h1 {
    color: #4c4c4c;
    font-size: 1.3rem;
    font-style: normal;
    font-weight: 600;
    line-height: normal;
  }
  p {
    color: #303030;
    font-size: 1rem;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
`;
export default function UserProfileComment({ data }) {
  const { text, review, created_date } = data;
  const { monthNumber, date, year, hour, minute } = extractDate(created_date);

  return (
    <Container>
      <NameAndDate>
        <h1>Review {review.id}</h1>
        <p>{`${date}.${monthNumber}.${year}  ${hour}:${minute}`}</p>
      </NameAndDate>
      <Comment>{text}</Comment>
    </Container>
  );
}
