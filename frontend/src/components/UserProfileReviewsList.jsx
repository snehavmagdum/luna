import styled from "styled-components";
import {BoldUpperCaseTittle} from "../styles/BoldUpperCaseTittle";
import UserReview from "./UserProfileReview";
import {useDispatch, useSelector} from "react-redux";
import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import {api} from "../axios/index.js";
import {setSelectedProfile, setSelectedProfileReviews} from "../store/slices/profile.js";
import Loading from "./Loading.jsx";

const ReviewStyle = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 5px;
  gap: 2rem;
  flex-grow: 1
`;

export default function UserProfileReviewsList() {
    const profileReviews = useSelector((state) => state.profile.selectedProfileReviews);
    const token = useSelector((state) => state.user.accessToken);
    const {user_id} = useParams();
    const [loading, setLoading] = useState(true);
    const dispatch = useDispatch();

    const loadUserReviews = async () => {
        let headers = {}
        if (token) {
            headers = {Authorization: `Bearer ${token}`}
        }
        try {
            const res = await api.get(`reviews/user/${user_id}`, {headers})
            dispatch(setSelectedProfileReviews(res.data))
            setLoading(false)
        } catch (e) {
            console.log(e)
        }
    }

    useEffect(() => {
        loadUserReviews()
    }, []);

    if (loading) {
        return <Loading/>;
    }

    return (
        <ReviewStyle>
            <BoldUpperCaseTittle>Reviews</BoldUpperCaseTittle>
            {profileReviews.map(review => <UserReview key={review.id} data={review}/>)}
        </ReviewStyle>
    );
}
