import styled from "styled-components";
import UserCard from "./UserCard.jsx";
import {api} from "../axios/index.js";
import {useEffect, useState} from "react";
import Loading from "./Loading.jsx";
import Empty from "./Empty.jsx";

const Container = styled.div`
  display: grid;
  grid-template-columns:1fr 1fr 1fr 1fr;
  row-gap: 2rem;
  column-gap: 2rem;
  margin-top: 42px;
  margin-bottom: 100px;
`;

export default function UserCardContainer() {
    const [userList, setUserList] = useState([]);
    const [loading, setLoading] = useState(true)
    const fetchReviews = async () => {
        try {
            const response = await api.get("/users/");
            setUserList(response.data);
            setLoading(false)
        } catch (e) {
            console.log(e);
        }
    };

    useEffect(() => {
        fetchReviews();
    }, []);

    if (loading) {
        return <Loading/>
    } else if (userList.length > 0) {
        return <Container>
            {
                userList.map(user => {
                    return <UserCard key={user} user={user}></UserCard>
                })
            }
        </Container>;

    } else {
        return (
            <Empty message={"No users found. Please refine your search."}/>
        )
    }

}
