import styled from "styled-components";
import {NavLink} from "react-router-dom";

const NavGroup = styled.div`
  width: 630px;
  height: 47px;
  flex-shrink: 0;
  margin-right: 10px;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
`;

const NavItem = styled(NavLink)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: center;
  color: #4a4a4a;
  height: 100%;
  text-decoration: none;
  text-transform: uppercase;
  position: relative;
  transition: border-bottom-color 0.3s ease;

  &.active {
    font-weight: bold;

    &:after {
      content: "";
      position: absolute;
      bottom: -2px;
      left: 0;
      width: 100%;
      height: 2px;
      background-color: #f07c3e;
    }
  }
`;

export default function SubheaderNavigation() {
    // some other code here

    return <NavGroup>
        <NavItem to="/search/restaurants">
            Restaurants
        </NavItem>
        <NavItem to="/search/reviews">
            Reviews
        </NavItem>
        <NavItem to="/search/users">
            Users
        </NavItem>
    </NavGroup>;
}
