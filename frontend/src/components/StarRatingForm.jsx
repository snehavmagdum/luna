import React, { useState } from "react";
import styled from "styled-components";

const StarContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  width: 148px; // 32px (star size) * 5 - 2
  color: grey;
  position: relative;
  transition: width 0.3s ease-in-out; // Add transition property for smooth animation
`;

const FilledStars = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: ${(props) => (props.score / 5) * 100}%;
  overflow: hidden;
  color: #f8e71c;
`;

const Star = styled.span`
  font-size: 27px;
  margin-right: 3px;
  cursor: pointer;
`;

function StarRatingForm({ score = 0 }) {
  const [hoveredStar, setHoveredStar] = useState(null);
  const starArray = Array(5).fill(true);

  const handleStarHover = (index) => {
    setHoveredStar(index + 1);
  };

  return (
    <StarContainer>
      {starArray.map((star, index) => (
        <Star
          key={index}
          onMouseEnter={() => handleStarHover(index)}
          onMouseLeave={() => setHoveredStar(null)}
          onClick={() => props.setSelectedRating(index + 1)}
        >
          &#9733;
        </Star>
      ))}
      <FilledStars score={hoveredStar || score}>
        {starArray.map((star, index) => (
          <Star key={index}>&#9733;</Star>
        ))}
      </FilledStars>
    </StarContainer>
  );
}

export default StarRatingForm;
