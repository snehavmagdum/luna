import styled from "styled-components";
import RestaurantCard from "./RestaurantCard.jsx";
import {useDispatch, useSelector} from "react-redux";
import {useEffect, useState} from "react";
import {setSearchedRestaurant, setSearchField} from "../store/slices/restaurant.js";
import {api} from "../axios/index.js";
import Empty from "./Empty.jsx";
import Loading from "./Loading.jsx";
import {useSearchParams} from "react-router-dom";

const Container = styled.div`
  display: grid;
  grid-template-columns:repeat(4, 1fr);
  gap: 30px;
  margin-top: 42px;
  margin-bottom: 100px;
`;

export default function CardContainer() {
    const restaurantList = useSelector((state) => state.restaurant.restaurants)
    const searchString = useSelector((state) => state.restaurant.searchField)
    const categoryString = useSelector((state) => state.restaurant.categoryField)
    const searchedRestaurantList = useSelector((state) => state.restaurant.searchedRestaurant)
    const dispatch = useDispatch()
    const [loading, setLoading] = useState(true)
    const [searchParams] = useSearchParams();
    const searchQueryParam = searchParams.get('s');
    let term = ""

    const fetchRestaurants = async () => {
        if (searchQueryParam) {
            term = searchQueryParam
        } else {
            term = searchString
        }
        try {
            const response = await api.get("search/restaurants/", {
                params: {
                    search_term: term,
                    category: categoryString
                }
            });
            dispatch(setSearchedRestaurant(response.data));
            setLoading(false)
        } catch (e) {
            console.log(e);
        }
    };

    useEffect(() => {
        dispatch(setSearchField(searchQueryParam))
        fetchRestaurants();
    }, [searchQueryParam, searchString, categoryString]);

    if (loading) {
        return <Loading/>
    }
    else if (searchString || categoryString) {
        if (searchedRestaurantList.length === 0) {
            return (
                <Empty message={"No restaurants found. Please refine your search."}/>
            )
        } else {
            return (
                <Container>
                    {searchedRestaurantList.map(rest => {
                        return <RestaurantCard key={rest.id} data={rest}/>
                    })}
                </Container>
            )
        }
    } else if (restaurantList.length > 0) {
        return (
            <Container>
                {restaurantList.map(rest => {
                    return <RestaurantCard key={rest.id} data={rest}/>
                })}
            </Container>
        )
    } else {
        return (
            <Empty message={"No restaurants found."} />
        )
    }
}
