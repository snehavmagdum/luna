import styled from 'styled-components';
import { useRef } from 'react';
import { api } from '../axios/index.js';
import { useDispatch, useSelector } from 'react-redux';
import { setSelectedProfile } from '../store/slices/profile.js';
import {loadUserDetails} from "../store/slices/user.js";

const AvatarContainer = styled.div`
  width: 230px;
  height: 230px;
`;

const AvatarContainerEdit = styled(AvatarContainer)`
  position: relative;
  cursor: pointer;
`;

const AvatarDisplay = styled.div`
  width: 100%;
  height: 100%;
  background-size: cover;
`;

const Overlay = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.4);
  display: flex;
  align-items: center;
  justify-content: center;
  opacity: 0;
  transition: opacity 0.3s ease;
  z-index: 1;
  color: white;

  svg {
    font-size: 2rem;
    color: white;
  }

  ${AvatarContainerEdit}:hover & {
    opacity: 1;
  }
`;

export default function UserProfileAvatar() {
  const user = useSelector((state) => state.profile.selectedProfile);
  const token = useSelector((state) => state.user.accessToken);
  const fileInput = useRef();
  const dispatch = useDispatch();
  const { avatar, logged_in_user_is_self } = user;

  const handleAvatarClick = () => {
    fileInput.current.click();
  };

  const handleFileChange = async (event) => {
    const formData = new FormData();
    const imageFile = event.target.files[0];

    if (imageFile) {
      formData.append('avatar', imageFile);
    }

    try {
      const res = await api.patch('/me/', formData, {
        headers: { Authorization: 'Bearer ' + token },
      });
      dispatch(setSelectedProfile(res.data));
      dispatch(loadUserDetails(res.data));
    } catch (e) {
      console.log(e.response.data);
    }
  };

  return logged_in_user_is_self ? (
    <AvatarContainerEdit onClick={handleAvatarClick}>
      <Overlay>
        Edit
      </Overlay>
      <AvatarDisplay
        style={{
          backgroundImage: `url(${avatar ? avatar : '/profile.jpg'})`,
        }}
      />
      <input
        type='file'
        ref={fileInput}
        accept="image/*"
        style={{ display: 'none' }}
        onChange={handleFileChange}
      />
    </AvatarContainerEdit>
  ) : (
    <AvatarContainer>
      <AvatarDisplay
        style={{
          backgroundImage: `url(${avatar ? avatar : '/profile.jpg'})`,
        }}
      />
    </AvatarContainer>
  );
}
