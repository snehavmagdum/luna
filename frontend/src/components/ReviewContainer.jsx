import styled from "styled-components";
import ReviewCard from "./ReviewCard.jsx";
import {api} from "../axios/index.js";
import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {setSearchedReview} from "../store/slices/review.js";
import Loading from "./Loading.jsx";
import Empty from "./Empty.jsx";

const Container = styled.div`
  display: grid;
  grid-template-columns:repeat(4, 1fr);
  gap: 30px;
  margin-top: 42px;
  margin-bottom: 100px;
`;

export default function ReviewContainer() {
    const reviewList = useSelector((state) => state.review.review)
    const searchString = useSelector((state) => state.review.searchField)
    const searchedReviewList = useSelector((state) => state.review.searchedReview)
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(true)

    const fetchReviews = async () => {
        try {
            const response = await api.get("/search/reviews/", {
                params: {
                    search_term: searchString
                }
            });
            dispatch(setSearchedReview(response.data))
            setLoading(false)
        } catch (e) {
            console.log(e);
        }
    };

    useEffect(() => {
        fetchReviews();
    }, [searchString]);

    if (loading) {
        return <Loading/>
    } else if (searchString) {
        if (searchedReviewList.length === 0) {
            return (
                <Empty message={"No reviews found. Please refine your search."}/>
            )
        } else {
            return (
                <Container>
                    {searchedReviewList.map(review => {
                        return <ReviewCard key={review.id} data={review}></ReviewCard>
                    })}
                </Container>
            )
        }
    } else if (reviewList.length > 0) {
        return (
            <Container>
                {reviewList.map(review => {
                    return <ReviewCard key={review.id} data={review}></ReviewCard>
                })}
            </Container>
        )
    } else {
        return (
            <Empty message={"No reviews found."}/>
        )
    }
}
