import styled from "styled-components";
import { NavLink, useNavigate } from "react-router-dom";
import useLogin from "../hooks/useLogin.js";
import { useSelector } from "react-redux";

const HeaderWrapper = styled.div`
  width: 100%;
  height: 4rem;
  padding: 1rem 1.75rem;
  display: flex;
  justify-content: space-between;
  gap: 40px;
`;

const HeaderLeft = styled.div`
  width: 100vw;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const Logo = styled(NavLink)`
  display: flex;
  width: 144px;
  flex-direction: column;
  justify-content: center;
  flex-shrink: 0;
  color: #000;
  font-size: 36px;
  font-weight: 700;
  line-height: normal;
  cursor: pointer;
  text-decoration: none;
`;

const NavGroup = styled.div`
  width: 240px;
  margin-right: 10px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const NavItem = styled(NavLink)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: center;
  color: #4a4a4a;
  height: 100%;
  text-decoration: none;
  position: relative;
  transition: border-bottom-color 0.3s ease;

  &.active {
    font-weight: bold;

    &:after {
      content: "";
      position: absolute;
      bottom: -2px;
      left: 0;
      width: 100%;
      height: 2px;
      background-color: #f07c3e;
    }
  }
`;

const ButtonGroup = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  width: 15%;
  gap: 1px;
  text-transform: uppercase;
`;

const ButtonAlone = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  text-decoration: none;
  background-color: #e47d31;
  color: #fff;
  width: 100px;
  height: 40.571px;
  border-radius: 20px;
  border: none;
  cursor: pointer;
  text-transform: uppercase;
`;

const ButtonLeft = styled(NavLink)`
  display: flex;
  align-items: center;
  justify-content: center;
  text-decoration: none;
  background-color: #e47d31;
  color: #fff;
  width: 100px;
  height: 40.571px;
  flex-shrink: 0;
  border-bottom-left-radius: 20px;
  border-top-left-radius: 20px;
  border: none;
  cursor: pointer;
`;

const ButtonRight = styled(NavLink)`
  display: flex;
  align-items: center;
  justify-content: center;
  text-decoration: none;
  background-color: #e47d31;
  color: #fff;
  width: 100px;
  height: 40.571px;
  flex-shrink: 0;
  border-bottom-right-radius: 20px;
  border-top-right-radius: 20px;
  border: none;
  cursor: pointer;
`;

export default function Header() {
  const isLoggedIn = useSelector((state) => state.user.accessToken);
  const loggedinUser = useSelector((state) => state.user.details);

  const { handleLogout } = useLogin();

  return (
    <HeaderWrapper>
      <HeaderLeft>
        <Logo to="/">LUNA</Logo>
        <NavGroup>
          <NavItem to="/">Home</NavItem>
          <NavItem to="/search/restaurants">Search</NavItem>
          {loggedinUser ? (
            <NavItem to={`/profile/${loggedinUser.id}/reviews`}>Profile</NavItem>
          ) : (
            <></>
          )}
        </NavGroup>
      </HeaderLeft>
      {isLoggedIn ? (
        <ButtonGroup>
          <ButtonAlone onClick={(e) => handleLogout()}>Logout</ButtonAlone>
        </ButtonGroup>
      ) : (
        <ButtonGroup>
          <ButtonLeft to="/register">Signup</ButtonLeft>
          <ButtonRight to="/login">Login</ButtonRight>
        </ButtonGroup>
      )}
    </HeaderWrapper>
  );
}

export class navigateToSearch {}
