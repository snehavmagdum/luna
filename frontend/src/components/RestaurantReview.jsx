import styled from "styled-components";
import StarRating from "./StarRating.jsx";
import { useState } from "react";
import UserComment from "./UserComment.jsx";
import { useDispatch, useSelector } from "react-redux";
import { api } from "../axios/index.js";
import SubButton from "./SubButton.jsx";
import { extractDate } from "../../utils/extractDate.js";

const UserBox = styled.div`
  background-color: white;
  margin-bottom: 20px;
`;

const UserInformation = styled.div`
  color: #000;
  display: flex;
  flex-direction: row;
  justify-content: stretch;
  font-size: 20px;
  font-style: normal;
  font-weight: 300;
  line-height: normal;
  height: 68px;
  border-bottom: #767676 1px;
  width: 100%;
`;

const DateCreated = styled.div`
  color: #000;
  text-align: right;
  font-size: 14px;
  font-style: normal;
  font-weight: 300;
  line-height: normal;
`;

const UserNameAndReviews = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: start;
`;

const ReviewsTotal = styled.div`
  color: #4c4c4c;
  font-size: 14px;
  font-style: normal;
  font-weight: 700;
  line-height: normal;
`;

const Name = styled.div`
  color: #e47d31;
  font-size: 20px;
  font-style: normal;
  font-weight: 700;
  line-height: normal;
`;

const UserImage = styled.div`
  width: 68px;
  height: 68px;
  background-color: gray;
  background-size: cover;
`;

const UserInformationTab = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 10px;
`;

const UserInformationBox = styled.div`
  display: flex;
  flex-direction: column;
  border-bottom: 1px solid #c7c7c7;
  width: 100%;
  height: 100%;
`;

const ReviewText = styled.div`
  color: #000;
  font-size: 16px;
  font-style: normal;
  font-weight: 300;
  line-height: normal;
  margin-left: 20px;
  margin-right: 20px;
  margin-top: 20px;
`;

const ReviewFooter = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-top: 10px;
`;

const ReviewCommentingForm = styled.form`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-top: 10px;
  padding: 10px;
`;

const ReviewCommentInput = styled.input`
  min-width: 50%;
  height: 30px;
  padding: 10px;
  border-radius: 3px;
  border: 1px solid #ebebeb;
  background: #fff;

  :focus {
    outline: none;
  }
`;

const ReplyButtons = styled.div`
  width: 249px;
  height: 33px;
  flex-shrink: 0;
  border-radius: 28px;
  background: rgba(145, 145, 145, 0.6);
  display: flex;
  flex-direction: row;
  padding-left: 20px;
  padding-right: 20px;
  align-items: center;
  margin-top: 30px;
  margin-left: 20px;
  margin-bottom: 20px;
`;

const LikeButton = styled.div`
  width: 120px;
  color: white;
  user-select: none;
  cursor: pointer;
`;

const CommentButton = styled.div`
  width: 120px;
  text-align: right;
  color: white;
  user-select: none;
  cursor: pointer;
`;

const ReviewCommentsContainer = styled.div`
  width: 100%;
`;

const UserCommentItemContainer = styled.div`
  width: 100%;
  padding: 10px;
  border-bottom: 1px rgba(0, 0, 0, 0.2) solid;
  display: flex;
  justify-content: space-between;
  align-items: start;
`;

const CommentCreatedDate = styled.div`
  color: #000;
  text-align: right;
  font-size: 12px;
  font-style: normal;
  font-weight: 300;
  line-height: normal;
`;

const ViewAllComments = styled.div`
  color: #e47d31;
  text-align: right;
  font-size: 16px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  margin-right: 20px;
  cursor: pointer;
  user-select: none;
`;

const Splitter = styled.div`
  width: 2px;
  height: 100%;
  background-color: #ffffff;
`;

function commentDate(inputDate) {
  const { monthNumber, date, year, hour, minute } = extractDate(inputDate);
  return `${date}.${monthNumber}.${year}  ${hour}:${minute}`;
}

export default function RestaurantReview({ review }) {
  const token = useSelector((state) => state.user.accessToken);
  const [currentReview, setCurrentReview] = useState(review);
  const [toggleComments, setToggleComments] = useState(false);
  const [commenting, setCommenting] = useState(false);
  const [comment, setComment] = useState("");

  if (!review) {
    return false;
  }

  const { id, user, rating, created_date, text, comments, likes_count } =
    currentReview;

  const { monthNumber, date, year, hour, minute } = extractDate(created_date);

  const toggleLike = async () => {
    if (!token) {
      return false;
    }

    try {
      const res = await api.post(
        `reviews/like/${id}`,
        {},
        { headers: { Authorization: "Bearer " + token } }
      );
      setCurrentReview(res.data);
    } catch (e) {
      console.log(e);
    }
  };

  const toggleCommenting = () => {
    if (!token) {
      return false;
    } else setCommenting(!commenting);
  };

  const handleCommenting = async (e) => {
    e.preventDefault();

    if (!token) {
      return false;
    }

    try {
      const res = await api.post(
        `reviews/${id}/comment`,
        { text: comment },
        { headers: { Authorization: "Bearer " + token } }
      );
      setCurrentReview(res.data);
      setToggleComments(true);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <UserBox>
      <UserInformationBox>
        <UserInformation>
          <UserImage
            style={{
              backgroundImage: `
            url(${user.avatar ? user.avatar : "/profile.jpg"})`,
            }}
          />
          <UserInformationTab>
            <UserNameAndReviews>
              <Name>
                {user.first_name} {user.last_name}
              </Name>
              <ReviewsTotal>{user.num_reviews} Reviews in total</ReviewsTotal>
            </UserNameAndReviews>
            <StarRating score={rating} />
            <DateCreated>{`${date}.${monthNumber}.${year}  ${hour}:${minute}`}</DateCreated>
          </UserInformationTab>
        </UserInformation>
      </UserInformationBox>
      <ReviewText>{text}</ReviewText>
      {commenting ? (
        <ReviewCommentingForm onSubmit={(e) => handleCommenting(e)}>
          <ReviewCommentInput
            onChange={(e) => setComment(e.target.value)}
            value={comment}
          />
          <SubButton type="submit">Post</SubButton>
          <ViewAllComments onClick={() => setCommenting(false)}>
            Hide
          </ViewAllComments>
        </ReviewCommentingForm>
      ) : (
        <ReviewFooter>
          <ReplyButtons>
            <LikeButton onClick={() => toggleLike()}>
              Like {likes_count}
            </LikeButton>
            <Splitter />
            <CommentButton onClick={() => toggleCommenting()}>
              Comment {comments.length}
            </CommentButton>
          </ReplyButtons>
          {comments.length > 0 && (
            <ViewAllComments onClick={() => setToggleComments(!toggleComments)}>
              {toggleComments ? (
                <>Hide comments</>
              ) : (
                <>Show all {comments.length} comments</>
              )}
            </ViewAllComments>
          )}
        </ReviewFooter>
      )}

      {toggleComments ? (
        <ReviewCommentsContainer>
          {comments.map((comment) => (
            <UserCommentItemContainer key={comment.id}>
              <UserComment comment={comment} />
              <CommentCreatedDate>
                {commentDate(comment.created_date)}
              </CommentCreatedDate>
            </UserCommentItemContainer>
          ))}
        </ReviewCommentsContainer>
      ) : (
        <></>
      )}
    </UserBox>
  );
}