import styled from "styled-components";
import { NavLink, useMatch } from "react-router-dom";
import StarIcon from "../assets/star.svg";
import CommentIcon from "../assets/comment.svg";
import RestaurantIcon from "../assets/restaurant.svg";
import EditIcon from "../assets/edit.svg";
import {useSelector} from "react-redux";

const NavItem = styled(NavLink)`
  display: flex;
  flex-direction: row;
  justify-content: left;
  align-items: center;
  text-align: center;
  color: #4a4a4a;
  height: 46px;
  text-decoration: none;
  position: relative;
  gap: 0.3rem;
  
  border-bottom: 1px solid #817c7a;
  padding-left: 10px;
  transition: border-bottom-color 0.3s ease;
  
  &:first-child {
  border-top: 1px solid #817c7a;  
  }

  &.active {
    font-weight: bold;
    background-color: #f8f8f8;

    &:after {
      content: "";
      position: absolute;
      bottom: -2px;
      top: 0;
      left: -2px;
      width: 5px;
      height: 100%;
      background-color: #f07c3e;
    }
  }
`;

const NavGroup = styled.div`
  width: 230px;
  margin-right: 10px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const NavItemImage = styled.img`
  width: 24px;
  height: 24px;
  margin-bottom: 8px;
`;

export default function UserProfileNavigation() {
    const user = useSelector(state=>state.profile.selectedProfile)
  const {id, logged_in_user_is_self} = user

  return (
      <NavGroup>
        <NavItem to={`/profile/${id}/reviews`}>
          <NavItemImage src={StarIcon} />
          Reviews
        </NavItem>
        <NavItem to={`/profile/${id}/comments`}>
          <NavItemImage src={CommentIcon} />
          Comments
        </NavItem>
        <NavItem to={`/profile/${id}/restaurants`}>
          <NavItemImage src={RestaurantIcon} />
          Restaurants
        </NavItem>
        {logged_in_user_is_self? (<NavItem to={`/profile/${id}/edit`}>
          <NavItemImage src={EditIcon} />
          Edit profile
        </NavItem>):(<></>)}

      </NavGroup>
  );
}
