import styled from "styled-components";
import {BoldUpperCaseTittle} from "../styles/BoldUpperCaseTittle";
import UserProfileComment from "./UserProfileComment";
import {useDispatch, useSelector} from "react-redux";
import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import {api} from "../axios/index.js";
import {setSelectedProfileComments} from "../store/slices/profile.js";
import Loading from "./Loading.jsx";

const CommentStyle = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 5px;
  gap: 1rem;
  align-self: flex-start;
  flex-grow: 1;
`;

export default function UserProfileCommentsList() {
    const profileComments = useSelector((state) => state.profile.selectedProfileComments);
    const token = useSelector((state) => state.user.accessToken);
    const {user_id} = useParams();
    const [loading, setLoading] = useState(true);
    const dispatch = useDispatch();

    const loadUserComments = async () => {
        let headers = {}
        if (token) {
            headers = {Authorization: `Bearer ${token}`}
        }
        try {
            const res = await api.get(`comments/user/${user_id}`, {headers})
            dispatch(setSelectedProfileComments(res.data))
            setLoading(false)
        } catch (e) {
            console.log(e)
        }
    }

    useEffect(() => {
        loadUserComments()
    }, []);

    if (loading) {
        return <Loading/>;
    }

    return (
        <CommentStyle>
            <BoldUpperCaseTittle>Comments</BoldUpperCaseTittle>
            {profileComments.map(comment => <UserProfileComment key={comment.id} data={comment}/>)}
        </CommentStyle>
    );
}
