import styled from "styled-components";

const Container = styled.div`
  height: 40px;
  display: flex;
  flex-direction: column;
  gap: 0.2rem;
  width: 255px;
  flex-shrink: 0;
`;
const UserName = styled.div`
  height: 40%;
  color: #e47d31;
  font-size: 14px;
  font-style: normal;
  font-weight: 700;
  line-height: normal;
  overflow: clip;
`;

const Content = styled.div`
  height: 60%;
  color: #000;
  font-size: 14px;
  font-style: normal;
  font-weight: 300;
  line-height: normal;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;
export default function UserComment({comment}) {
    return (
        <Container>
            <UserName>{comment.user.first_name} {comment.user.last_name}</UserName>
            <Content>{comment.text}</Content>
        </Container>
    );
}
