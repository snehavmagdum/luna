import styled from "styled-components";
import StarRating from "./StarRating.jsx";
import {useNavigate} from "react-router-dom";

const Card = styled.div`
  background-color: #fff;
  width: 272px;
  height: 400px;
  flex-shrink: 0;
  display: flex;
  flex-direction: column;
  gap: 0.2rem;
  border: 1px solid #EBEBEB;
  border-top: 5px solid #E47D31;
  cursor: pointer;
`;

const RestaurantName = styled.span`
  width: 100%;
  color: #4C4C4C;
  padding-left: 14px;
  padding-top: 11px;
  font-size: 20px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  overflow: clip;
`;

const RestaurantAddress = styled.div`
  width: 100%;
  height: 25px;
  color: #4C4C4C;
  padding-left: 14px;
  padding-top: 6px;
  font-size: 16px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  overflow: hidden;
  text-overflow: ellipsis;
  display: flex;
  gap: 0.5rem;
  
  span{
    height: 100%;
  }
`;

const Rating = styled.div`
  width: 100%;
  display: flex;
  padding: 14px;
  justify-content: space-between;
  align-items: center;
`;

const TotalRating = styled.div`
  font-size: 1.25rem;
  text-align: right;
  color: #4C4C4C;
  font-style: normal;
  font-weight: 300;
  line-height: normal;
`;

const RestaurantPicture = styled.div`
  background-size: cover;
  width: 270px;
  height: 270px;
`;

export default function RestaurantCard({data}) {
    const navigate = useNavigate()

    return <Card onClick={()=>navigate(`/restaurants/${data?.id}`)}>
        <RestaurantName>{data?.name}</RestaurantName>
        <RestaurantAddress>
            <span>{data?.city}, {data?.country_name}</span>
        </RestaurantAddress>
        <Rating>
            <StarRating score={data?.avg_rating} />
            <TotalRating>{data?.num_reviews}</TotalRating>
        </Rating>
        <RestaurantPicture style={{backgroundImage: `url(${data?.image})`}}/>
    </Card>;
}
