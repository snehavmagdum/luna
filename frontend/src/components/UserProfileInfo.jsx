import styled from "styled-components";
import { BoldUpperCaseTittle } from "../styles/BoldUpperCaseTittle";

const ProfileInfoStyle = styled.div`
  display: flex;
  flex-direction: column;
  gap: 1rem;
  align-self: flex-start;
  min-width: 10rem;
`;
const TitleParagraphWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 0.3rem;
`;

const BoldText = styled.div`
  font-weight: bold;
`;
const LightText = styled.div`
  font-weight: light;
`;

export default function UserProfileInfo({ ...props }) {
  const { first_name, location, member_since, things_i_love, description } =
    props;
  const dateObj = new Date(member_since);
  const month = new Intl.DateTimeFormat("en", { month: "long" }).format(
    dateObj
  );
  const year = dateObj.getFullYear();

  return (
    <ProfileInfoStyle>
      <BoldUpperCaseTittle>About {first_name}</BoldUpperCaseTittle>
      <TitleParagraphWrapper>
        <BoldText>Location</BoldText>
        <LightText>{location ? location : "-"}</LightText>
      </TitleParagraphWrapper>
      <TitleParagraphWrapper>
        <BoldText>Luna member since</BoldText>
        <LightText>{`${month}, ${year}`}</LightText>
      </TitleParagraphWrapper>
      <TitleParagraphWrapper>
        <BoldText>Things I Love</BoldText>
        <LightText>{things_i_love ? things_i_love : "-"}</LightText>
      </TitleParagraphWrapper>
      <TitleParagraphWrapper>
        <BoldText>Description</BoldText>
        <LightText>{description ? description : "-"}</LightText>
      </TitleParagraphWrapper>
    </ProfileInfoStyle>
  );
}
