import styled from "styled-components";

const SampleStyle = styled.div`
  color: red;`

export default function Sample() {
  // some other code here

  return <SampleStyle>Sample component</SampleStyle>;
}
