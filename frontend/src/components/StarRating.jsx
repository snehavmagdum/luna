import styled from 'styled-components';

const StarContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  width: 148px; // 32px (star size) * 5 - 2
  color: grey;
  position: relative;
`;

const FilledStars = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: ${props => (props.score / 5) * 100}%;
  overflow: hidden;
  color: #F8E71C;
`;

const Star = styled.span`
  font-size: 27px;
  margin-right: 3px;
`;

function StarRating({ score = 4 }) {
  const starArray = Array(5).fill(true);

  return (
    <StarContainer>
      {starArray.map((star, index) => (
        <Star key={index}>&#9733;</Star>
      ))}
      <FilledStars score={score}>
        {starArray.map((star, index) => (
          <Star key={index}>&#9733;</Star>
        ))}
      </FilledStars>
    </StarContainer>
  );
}

export default StarRating;
