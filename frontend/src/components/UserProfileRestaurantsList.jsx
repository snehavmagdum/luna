import styled from "styled-components";
import { BoldUpperCaseTittle } from "../styles/BoldUpperCaseTittle";
import UserProfileRestaurent from "./UserProfileRestaurent";
import Button from "./Button";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { api } from "../axios/index.js";
import { setSelectedProfileRestaurants } from "../store/slices/profile.js";
import Loading from "./Loading.jsx";

const RestaurantStyle = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 5px;
  gap: 1.1rem;
  flex-grow: 1;
`;

export default function UserRestaurantsList({ children }) {
  const navigate = useNavigate();
  const profileRestaurants = useSelector(
    (state) => state.profile.selectedProfileRestaurants
  );
  const token = useSelector((state) => state.user.accessToken);
  const { user_id } = useParams();
  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch();

  const loadUserRestaurants = async () => {
    let headers = {};
    if (token) {
      headers = { Authorization: `Bearer ${token}` };
    }
    try {
      const res = await api.get(`restaurants/user/${user_id}`, { headers });
      dispatch(setSelectedProfileRestaurants(res.data));
      setLoading(false);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    loadUserRestaurants();
  }, []);

  if (loading) {
    return <Loading />;
  }
  function handleOnClick() {
    navigate("/profile/restaurants/new");
  }
  return (
    <RestaurantStyle>
      <BoldUpperCaseTittle>Restaurants</BoldUpperCaseTittle>
      {profileRestaurants.map((restaurant) => (
        <UserProfileRestaurent key={restaurant.id} data={restaurant} />
      ))}
      <Button onClick={(e) => handleOnClick()}>Create restaurant</Button>
    </RestaurantStyle>
  );
}
