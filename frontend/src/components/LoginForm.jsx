import styled from "styled-components";
import Button from "./Button";
import {SubheaderTitle} from "../styles/SubheaderTitle.js";
import {useSelector} from "react-redux";
import {useState} from "react";
import useLogin from "../hooks/useLogin.js";
import {ErrorMessage} from "../styles/ErrorMessage.js";

const LoginContainer = styled.div`
  display: flex;
  padding-top: 4rem;
  width: 100%;
  height: 80vh;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  gap: 4rem;
  background: #F8F8F8;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 2rem
`;

const LoginFormField = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: start;
  gap: 8px;
`;

const InputField = styled.input`
  width: 24.1875rem;
  height: 3.25rem;
  flex-shrink: 0;
  border-radius: 0.1875rem;
  border: 1px solid #ebebeb;
  background: #fff;
  padding-left: 1rem;
  outline: none;

  ::placeholder {
    color: #979797;
    font-size: 1.25rem;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
  }

  &:focus {
    border-color: #e47d31;
    box-shadow: 0 0 4px rgba(191, 79, 116, 0.5);
  }
`;


export default function LoginForm() {
    const accessToken = useSelector((state) => state.user.accessToken);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const {handleLogin, loginError} = useLogin(email, password);

    const handleLoginSubmit = async (e) => {
        e.preventDefault();
        console.log("Handle login submit")
        await handleLogin(email, password);
    };


    return <LoginContainer>
        <SubheaderTitle>Login</SubheaderTitle>
        <Form onSubmit={(e) => handleLoginSubmit(e)}>
            <LoginFormField>
                <InputField
                    placeholder="Email"
                    type="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
                <ErrorMessage>{loginError?.email}</ErrorMessage>

            </LoginFormField>
            <LoginFormField>
                <InputField
                    placeholder="Password"
                    type="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}/>
                <ErrorMessage>{loginError?.password}</ErrorMessage>

            </LoginFormField>
            <ErrorMessage>{loginError?.detail}</ErrorMessage>
            <Button type="submit">Login</Button>
        </Form>

    </LoginContainer>;
}
