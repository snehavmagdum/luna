import styled from "styled-components";

const LoadingContainer = styled.div`
   width: 100%;
  height: 40vh;
  display: flex;
  align-items: center;
  justify-items: center;
  color: #333;
  
  & p {
    width: 100%;
    height: fit-content;
    text-align: center;
  }`

export default function Loading() {
  // some other code here

  return <LoadingContainer><p>Loading...</p></LoadingContainer>;
}
