import styled from "styled-components";
import Button from "./Button";
import { useSelector, useDispatch } from "react-redux";
import { BoldUpperCaseTittle } from "../styles/BoldUpperCaseTittle";
import { useEffect, useState } from "react";
import { api } from "../axios/index.js";
import { loadUserDetails } from "../store/slices/user.js";
import { setSelectedProfile } from "../store/slices/profile.js";
import { ErrorMessage } from "../styles/ErrorMessage.js";
import { useNavigate } from "react-router-dom";
import useLogin from "../hooks/useLogin.js";

const EditProfileFormStyle = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  justify-content: flex-start;
  align-items: start;
  gap: 1rem;
  flex-grow: 1;
`;

const Form = styled.form`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  gap: 2rem;
`;

const FormFields = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  gap: 0.5rem;
  align-items: start;
`;

const InputStyle = styled.input`
  width: 24.1875rem;
  height: 2.8rem;
  flex-shrink: 0;
  border-radius: 0.1875rem;
  border: 1px solid #ebebeb;
  background: #fff;
  padding-left: 1rem;
  outline: none;

  ::placeholder {
    color: #979797;
    font-size: 0.8rem;
    font-style: normal;
    font-weight: 500;
    line-height: normal;
  }

  &:focus {
    border-color: #e47d31;
    box-shadow: 0 0 4px rgba(191, 79, 116, 0.5);
  }
`;

const TextAreaStyle = styled.textarea`
  width: 24.1875rem;
  height: 5rem;
  flex-shrink: 0;
  border-radius: 0.1875rem;
  border: 1px solid #ebebeb;
  background: #fff;
  padding-left: 1rem;
  outline: none;
  resize: none;
  padding-top: 12px;

  ::placeholder {
    color: #979797;
    font-size: 0.8rem;
    font-style: normal;
    font-weight: 500;
    line-height: normal;
  }

  &:focus {
    border-color: #e47d31;
    box-shadow: 0 0 4px rgba(191, 79, 116, 0.5);
  }
`;

const DeleteButton = styled.button`
  border: none;
  outline: none;
  color: red;
  font-weight: 500;
  background: none;
`;

const LabelStyle = styled.label`
  color: #979797;
  font-size: 1rem;
  font-style: normal;
  font-weight: 700;
  line-height: normal;
  margin-top: 10px;
`;

const ButtonPlacingStyle = styled.div`
  display: flex;
  justify-content: space-between;
  gap: 1rem;
`;

export default function UserProfileEdit() {
  const token = useSelector((state) => state.user.accessToken);
  const user = useSelector((state) => state.user.details);
  const selectedProfile = useSelector((state) => state.profile.selectedProfile);
  const [username, setUsername] = useState(user.username);
  const [firstName, setFirstName] = useState(user.first_name);
  const [lastName, setLastName] = useState(user.last_name);
  const [email, setEmail] = useState(user.email);
  const [location, setLocation] = useState(user.location);
  const [phone, setPhone] = useState(user.phone_number);
  const [thingsILove, setThingsILove] = useState(user.things_liked);
  const [description, setDescription] = useState(user.description);
  const [error, setError] = useState("");
  const navigate = useNavigate();
  const { handleLogout } = useLogin();
  const dispatch = useDispatch();

  useEffect(() => {
    if (!user) {
      navigate("/");
    } else if (user !== selectedProfile) {
      // if user tries to edit wrong profile, forward to own profile or home
      dispatch(setSelectedProfile(user));
      navigate(`/profile/${user.id}/edit`);
    }
  }, [user, selectedProfile]);

  const handleEditSubmission = async (e) => {
    e.preventDefault();

    // validate Forms
    let errors = {};
    if (!username) errors.username = "Please define a user name";
    if (!firstName) errors.first_name = "Please enter your first name";
    if (!lastName) errors.last_name = "Please enter your last name";

    setError(errors);

    let formData = {
      username: username,
      first_name: firstName,
      last_name: lastName,
      location: location,
      phone_number: phone,
      things_liked: thingsILove,
      description: description,
    };

    try {
      const res = await api.patch("/me/", formData, {
        headers: { Authorization: "Bearer " + token },
      });
      dispatch(loadUserDetails(res.data));
      dispatch(setSelectedProfile(res.data));
    } catch (e) {
      setError(e.response);
    }
  };

  const handleDeleteSubmission = async (e) => {
    e.preventDefault();

    if (confirm("Are you sure to delete your profile?")) {
      try {
        const res = await api.delete("/me/", {
          headers: { Authorization: "Bearer " + token },
        });
        handleLogout();
      } catch (e) {
        setError(e.response);
      }
    }
  };

  return (
    <EditProfileFormStyle>
      <BoldUpperCaseTittle>Edit user profile</BoldUpperCaseTittle>
      <Form onSubmit={(e) => handleEditSubmission(e)}>
        <FormFields>
          <LabelStyle htmlFor="username">Username</LabelStyle>
          <InputStyle
            id="username"
            type="text"
            placeholder="Username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
          <ErrorMessage>{error?.username}</ErrorMessage>

          <LabelStyle htmlFor="firstname">First name</LabelStyle>
          <InputStyle
            id="firstname"
            type="text"
            placeholder="First name"
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
          />
          <ErrorMessage>{error?.first_name}</ErrorMessage>

          <LabelStyle htmlFor="lastname">Last name</LabelStyle>

          <InputStyle
            id="lastname"
            type="text"
            placeholder="Last name"
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
          />
          <LabelStyle htmlFor="email">E-mail</LabelStyle>

          <InputStyle
            id="email"
            placeholder="E-Mail Address"
            type="email"
            value={email}
            disabled={true}
            style={{ cursor: "not-allowed" }}
            onChange={(e) => setEmail(e.target.value)}
          />
          <LabelStyle htmlFor="location">Location</LabelStyle>

          <InputStyle
            id="location"
            type="text"
            placeholder="Location"
            value={location}
            onChange={(e) => setLocation(e.target.value)}
          />
          <LabelStyle htmlFor="phone">Phone</LabelStyle>

          <InputStyle
            id="phone"
            placeholder="Phone"
            type="tel"
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
          />
          <LabelStyle htmlFor="thingsILove">Things I Love</LabelStyle>

          <InputStyle
            id="thingsILove"
            type="text"
            placeholder="Things I Love"
            value={thingsILove}
            onChange={(e) => setThingsILove(e.target.value)}
          />
          <LabelStyle htmlFor="description">Description</LabelStyle>

          <TextAreaStyle
            id="description"
            type="text"
            placeholder="Description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
        </FormFields>

        <ButtonPlacingStyle>
          <Button type="submit">Save</Button>
          <DeleteButton onClick={(e) => handleDeleteSubmission(e)}>
            Delete account
          </DeleteButton>
        </ButtonPlacingStyle>
      </Form>
    </EditProfileFormStyle>
  );
}
