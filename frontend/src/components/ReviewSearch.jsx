import styled from "styled-components";
import ReviewContainer from "./ReviewContainer.jsx";
import {api} from "../axios/index.js";
import {setReviews, setSearchedField} from "../store/slices/review.js";
import {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";


const Container = styled.div`
  width: 75vw;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 10px;
`;

const InputSearch = styled.input.attrs({
    type: "text",
})`
  width: 1178px;
  height: 50px;
  flex-shrink: 0;
  border-radius: 0.1875rem;
  border: 1px solid #ebebeb;
  background: #fff;
  padding-left: 1rem;
  outline: none;

  ::placeholder {
    color: #D8D8D8;
    font-size: 20px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
  }

  &:focus {
    border-color: #e47d31;
    box-shadow: 0 0 4px rgba(191, 79, 116, 0.5);
  }
`;


export default function ReviewSearch() {
    //const [searchField,setSearchField]=useState("");
    const searchField = useSelector(state => state.review.searchField)
    const dispatch = useDispatch();
    const fetchReviews = async () => {
        try {
            const response = await api.get("/reviews/");
            dispatch(setReviews(response.data))

        } catch (e) {
            console.log(e);
        }
    };

    useEffect(() => {
        fetchReviews();
    }, []);


    return <Container>
        <InputSearch placeholder='Search Review..' value={searchField}
                     onChange={(e) => dispatch(setSearchedField(e.target.value))}></InputSearch>
        <ReviewContainer/>
    </Container>;
}
