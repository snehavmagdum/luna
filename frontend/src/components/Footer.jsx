import styled from 'styled-components';
import {GlobalStyle} from '../styles/global.js';
import faceBookLogo from '../assets/facebook.svg';
import twitterLogo from '../assets/twitter.svg';
import googleLogo from '../assets/googleplus.svg';
import instagramLogo from '../assets/instagram.svg';

const FooterContainer = styled.div`
  position: fixed;
  bottom: 0;
  width: 100%;
  height: 91px;
  background-color: #ffffff;
  z-index: 100;
`;

const FooterRow1 = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-start;
  padding: 0 0 0 0;
  width: 100%;
  height: 45%;
  background-color: #ffffff;
  margin-left: 20px;
`;

const FooterUl = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
  display: flex;
  flex-direction: row;
`;

const FooterLi = styled.li`
  margin-right: 25px;
  color: #646363;
  text-align: center;
  font-size: 20px;
  font-family: Roboto, serif;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  cursor: pointer;
`;

const FooterSplitter = styled.div`
  width: 100%;
  height: 3.158px;
  background-color: #EBEBEB;
`;

const SocialMedia = styled.div`
  display: flex;
  align-self: flex-end;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 200px;
  margin-right: 40px;
  height: 40px;
  margin-top: -70px;
`;

const SocialMediaIcon = styled.img`
  height: 35px;
  margin-top: -15px;
  cursor: pointer;
`;

const CopyRight = styled.p`
  color: #646363;
  font-size: 12px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  padding-left: 20px;
  margin-top: 15px;
`;

const Footer = () => {
    return (
        <FooterContainer>
            <GlobalStyle/>
            <FooterRow1>
                <nav>
                    <FooterUl>
                        <FooterLi>
                            <p>About Us</p>
                        </FooterLi>
                        <FooterLi>
                            <p>Press</p>
                        </FooterLi>
                        <FooterLi>
                            <p>Blog</p>
                        </FooterLi>
                        <FooterLi>
                            <p>iOS</p>
                        </FooterLi>
                        <FooterLi>
                            <p>Android</p>
                        </FooterLi>
                    </FooterUl>
                </nav>
                <SocialMedia>
                    <SocialMediaIcon src={faceBookLogo} alt="facebook"/>
                    <SocialMediaIcon src={twitterLogo} alt="twitter"/>
                    <SocialMediaIcon src={googleLogo} alt="google"/>
                    <SocialMediaIcon src={instagramLogo} alt="instagram"/>
                </SocialMedia>
            </FooterRow1>
            <FooterSplitter>
            </FooterSplitter>
            <CopyRight>© Copyright Luna 2023</CopyRight>
        </FooterContainer>
    );
};

export default Footer;
