import styled from "styled-components";
import UserProfileShort from "./UserProfileShort.jsx";
import UserComment from "./UserComment.jsx";
import LikeButtonImage from "../assets/LikeButtonImage.svg"
import {useNavigate} from "react-router-dom";


const ReviewCardContainer = styled.div`
  background-color: #fff;
  width: 272px;
  height: 400px;
  flex-shrink: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 0.7rem;
  border: 1px solid #EBEBEB;
  border-top: 5px solid #E47D31;
  padding-bottom: 0.5rem;
  overflow: clip;
  cursor: pointer;
`;

const RestaurantName = styled.span`
  display: flex;
  //align-self: flex-start;
  width: 255px;
  color: #E47D31;
  font-size: 18px;
  font-style: normal;
  font-weight: 700;
  line-height: normal;
`;

const Review = styled.div`
  width: 255px;
  height: 100px;
  color: #4C4C4C;
  margin-bottom: 2px;

  p {
    height: 85px;
    font-size: 14px;
    font-style: normal;
    font-weight: 700;
    line-height: 21px;
    overflow: hidden;


  }

  a {
    color: #E47D31;
    text-decoration: none;
    display: inline;
  }
`;

const Buttons = styled.div`
  display: flex;
  width: 255px;
  gap: 1px;
`;

const ButtonLeft = styled.button`
  background: rgba(145, 145, 145, 0.60);
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  color: #fff;
  width: 125px;
  height: 33px;
  flex-shrink: 0;
  border-bottom-left-radius: 20px;
  border-top-left-radius: 20px;
  border: none;
`;

const ButtonRight = styled.button`
  background: rgba(145, 145, 145, 0.60);

  color: #fff;
  width: 125px;
  height: 33px;
  flex-shrink: 0;
  border-bottom-right-radius: 20px;
  border-top-right-radius: 20px;
  border: none;
`;


const CommentContainer = styled.div`
  width: 95%;
  height: 30px;
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 0.5rem;


`;

const Text = styled.span`
  width: 100%;
  display: flex;
  align-self: flex-start;
  font-size: 20px;
  font-style: normal;
  font-weight: 300;
  line-height: normal;
`;


const NoComments = styled.p`
  font-size: 0.8rem;
  color: rgba(0, 0, 0, 0.5);
`

export default function ReviewCard({data}) {
    const navigate = useNavigate()

    return <ReviewCardContainer onClick={()=>navigate(`/restaurants/${data?.restaurant.id}#review-${data?.id}`)}>
        <UserProfileShort userInfo={data.user}></UserProfileShort>
        <RestaurantName>{data?.restaurant.name}</RestaurantName>
        <Review><p>{data?.text}</p><a
            href="#">read more</a></Review>
        <Buttons>
            <ButtonLeft>
                <img src={LikeButtonImage} alt="Button Icon"/>
                <span>Likes {data.likes_count}</span>
            </ButtonLeft>
            <ButtonRight>Comments {data.comments.length}</ButtonRight>
        </Buttons>
        <CommentContainer>
            <Text>Latest comments</Text>
            {data.comments.length > 0 ? (
                data.comments?.map((comment) => {
                    return <UserComment key={comment.id} comment={comment}/>
                })
            ) : (<NoComments>No comments yet</NoComments>)}
        </CommentContainer>

    </ReviewCardContainer>;
}
