import styled from "styled-components";
import {useDispatch, useSelector} from "react-redux";
import {useEffect, useState} from "react";
import {setCategoryField, setSearchField} from "../store/slices/restaurant.js";
import {api} from "../axios/index.js";
import {useSearchParams} from "react-router-dom";

const SearchContainer = styled.div`
  display: flex;
  width: 1178px;
  height: 50px;
  flex-shrink: 0;
  gap:10px;
  margin-top: 20px;
`;

const InputSearch = styled.input.attrs({
  type: "text",
})`
  width: 80%;
  height: 3.25rem;
  flex-shrink: 0;
  border-radius: 0.1875rem;
  border: 1px solid #ebebeb;
  background: #fff;
  padding-left: 1rem;
  outline: none;
  color: #4C4C4C;
  font-size: 20px;
  font-style: normal;
  font-weight: 400;
  line-height: 20px;

  ::placeholder {
    color: #979797;
  }

  &:focus {
    border-color: #e47d31;
    box-shadow: 0 0 4px rgba(191, 79, 116, 0.5);
  }
`;

const InputCategory = styled.div`
  width: 20%;
  height: 50px;

  select {
    width: 100%;
    height: 100%;
    font-size: 20px;
    font-style: normal;
    font-weight: 100;
    line-height: normal;
    border-radius: 0.1875rem;
    border: 1px solid #ebebeb;
  }

  select:focus {
    outline: #e47d31;
    box-shadow: 0 0 4px rgba(191, 79, 116, 0.5);
  }
`;


export default function InlineSearchbarRestaurantSearch() {
    const [categories, setCategories] = useState([]);
    const dispatch = useDispatch();
    const searchField = useSelector(state => state.restaurant.searchField)
    const categoryField = useSelector(state => state.restaurant.categoryField)

    const [searchParams, setSearchParams] = useSearchParams()

    useEffect(() => {
        fetchCategory();
    }, [])


    const fetchCategory = async () => {
        try {
            const response = await api.get("/categories/");
            setCategories(response.data)
            // TODO: Update state.constants.categories after fetching data
        } catch (e) {
            console.log(e);
        }
    };

    const updateSearchTerm = (e) => {
        setSearchParams({s: e.target.value})
        dispatch(setSearchField(e.target.value))
    }

    return <SearchContainer>
        <InputSearch placeholder='Search for a restaurant...' value={searchField}
                     onChange={(e) => updateSearchTerm(e)}></InputSearch>
        <InputCategory placeholder='Select a category'>
            <select value={categoryField} onChange={(e) => dispatch(setCategoryField(e.target.value))}>
                <option value="">Select category</option>
                {categories.map(category => {
                    return <option key={category.key} value={category.key}>{category.name}</option>
                })}
            </select>
        </InputCategory>
    </SearchContainer>;
}
