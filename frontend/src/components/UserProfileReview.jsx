import styled from "styled-components";
import StarRating from "./StarRating";
import { extractDate } from "../../utils/extractDate";

const SingleReviewWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;
const NameAndDate = styled.div`
  display: flex;
  justify-content: space-between;
  h1 {
    color: #4c4c4c;
    font-size: 1.25rem;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
  p {
    color: #303030;
    font-size: 1rem;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
`;

const ReviewText = styled.div`
  color: #303030;
  text-align: justify;
  font-size: 1rem;
  font-style: normal;
  font-weight: 300;
  line-height: normal;
  overflow: clip;
`;

export default function UserProfileReview({ data }) {
  const { created_date, text, rating, restaurant } = data;
  const { monthNumber, date, year, hour, minute } = extractDate(created_date);
  return (
    <SingleReviewWrapper>
      <NameAndDate>
        <h1>{restaurant.name}</h1>
        <p>{`${date}.${monthNumber}.${year}  ${hour}:${minute}`}</p>
      </NameAndDate>
      <StarRating score={rating} />
      <ReviewText>{text}</ReviewText>
    </SingleReviewWrapper>
  );
}
