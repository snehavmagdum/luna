import styled from "styled-components";

const ShortProfileStyle = styled.div`
  padding-left: 5px;
  color: #f3f8f5;
`;

const ShortProfileName = styled.p`
  font-size: 24px;
  font-weight: 700;
`
const ShortProfileInfo = styled.p`
  font-size: 18px;
  font-weight: 300;
`

export default function UserProfileShortInfo({...props}) {
    const {name, location, num_of_reviews, num_of_comments} = props;

    return (
        <ShortProfileStyle>
            <ShortProfileName>{name}</ShortProfileName>
            <ShortProfileInfo>{location}</ShortProfileInfo>
            <ShortProfileInfo>{num_of_reviews} reviews</ShortProfileInfo>
            <ShortProfileInfo>{num_of_comments} comments</ShortProfileInfo>
        </ShortProfileStyle>
    );
}
