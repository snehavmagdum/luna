import styled from "styled-components";
import giuse from "../assets/giuse.png"

const Container = styled.div`
  display: flex;
  gap: 0.5rem;
  width: 271px;
  height: 68px;
  flex-shrink: 0;
`;

const ProfilePicture = styled.div`
  width: 65px;
  height: 68px;
  flex-shrink: 0;
  background-color: #e5af31;
  background-size: cover;

`;

const UserDetails = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  flex-shrink: 0;
  gap: 0.5rem;
  width: 185.314px;
  height: 68px;

`;

const UserName = styled.div`
  color: #E47D31;
  font-size: 20px;
`;

const TotalReview = styled.div`
  color: #4C4C4C;
  font-size: 14px;
`;

export default function UserProfileShort({userInfo}) {
    return <Container>
        <ProfilePicture
            style={{backgroundImage: `url(${userInfo?.avatar ? userInfo?.avatar : '/profile.jpg'})`}}></ProfilePicture>
        <UserDetails>
            <UserName><span>{userInfo?.first_name} {userInfo?.last_name}</span></UserName>
            <TotalReview><span>{userInfo?.num_reviews} Reviews in total</span> </TotalReview>
        </UserDetails>
    </Container>;
}
