import styled from "styled-components";
import Button from "./Button";
import {SubheaderTitle} from "../styles/SubheaderTitle";
import {useSelector, useDispatch} from "react-redux";
import {resetRegistration, setRegistration} from "../store/slices/user";
import {api} from "../axios";
import {useNavigate} from "react-router-dom";
import {useState} from "react";
import {ErrorMessage} from "../styles/ErrorMessage.js";
import useLogin from "../hooks/useLogin.js";

const RegistrationVerificationFormStyle = styled.div`
  display: flex;
  padding-top: 4rem;
  width: 100%;
  height: 80vh;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  gap: 5rem;
  background: #f8f8f8;
`;

const Form = styled.form`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  gap: 2rem;
`;

const FormFields = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 2rem;
`;

const InputStyle = styled.input`
  width: 24.1875rem;
  height: 2.8rem;
  flex-shrink: 0;
  border-radius: 0.1875rem;
  border: 1px solid #ebebeb;
  background: #fff;
  padding-left: 1rem;
  outline: none;

  ::placeholder {
    color: #979797;
    font-size: 0.8rem;
    font-style: normal;
    font-weight: 500;
    line-height: normal;
  }

  &:focus {
    border-color: #e47d31;
    box-shadow: 0 0 4px rgba(191, 79, 116, 0.5);
  }
`;

const EmailField = styled(InputStyle)`
  color: #979797;
  font-weight: bold;
`;

export default function RegistrationVerificationForm() {
    const [error, setError] = useState("");
    const dispatch = useDispatch();
    let registration = useSelector((state) => state.user.registration);

    const {handleLogin} = useLogin(registration.email, registration.password)

    const navigate = useNavigate();

    const handleRegistrationComplete = () => {
        try {
            handleLogin(registration.email, registration.password);
        } catch (error) {
            console.log(error)
            navigate("/login")
        }
    };

    const validateRegistration = () => {
        // Make an API request to validate the registration data and generate the access token
        api
            .post("/registration/validate/", registration)
            .then(() => {
                dispatch(resetRegistration()); // Clear store after successful registration
                handleRegistrationComplete();
            })
            .catch((error) => {
                setError(error.response.data)
            });
    };

    const handleSubmission = (e) => {
        e.preventDefault()

        if (
            !registration.code ||
            !registration.first_name ||
            !registration.last_name ||
            !registration.location ||
            !registration.password ||
            !registration.password_repeat
        ) {
            setError({message: "Please fill in all fields"});
        } else if (registration.password !== registration.password_repeat) {
            setError({message: "Please enter two identical passwords"})
        } else {
            validateRegistration()
        }
    };


    return (
        <RegistrationVerificationFormStyle>
            <SubheaderTitle>Verification</SubheaderTitle>
            <Form onSubmit={e => handleSubmission(e)}>
                <FormFields>
                    <EmailField
                        placeholder="E-Mail Address"
                        value={registration.email}
                        type="email"
                        disabled={true}
                    />

                    <InputStyle
                        type="text"
                        pattern="[0-9]*"
                        value={registration.code}
                        placeholder="Validation code"
                        onChange={e => dispatch(setRegistration({code: e.target.value}))}
                    />
                    <InputStyle
                        type="text"
                        value={registration.first_name}
                        placeholder="First name"
                        onChange={e => dispatch(setRegistration({first_name: e.target.value}))}
                    />
                    <InputStyle
                        type="text"
                        value={registration.last_name}
                        placeholder="Last name"
                        onChange={e => dispatch(setRegistration({last_name: e.target.value}))}
                    />
                    <InputStyle
                        type="text"
                        value={registration.username}
                        placeholder="Username"
                        onChange={e => dispatch(setRegistration({username: e.target.value}))}
                    />
                    <InputStyle
                        type="text"
                        value={registration.location}
                        placeholder="Location"
                        onChange={e => dispatch(setRegistration({location: e.target.value}))}
                    />
                    <InputStyle
                        type="password"
                        value={registration.password}
                        placeholder="Password"
                        onChange={e => dispatch(setRegistration({password: e.target.value}))}
                    />
                    <InputStyle
                        type="password"
                        value={registration.password_repeat}
                        placeholder="Password repeat"
                        onChange={e => dispatch(setRegistration({password_repeat: e.target.value}))}
                    />
                </FormFields>
                <ErrorMessage>{error?.message}</ErrorMessage>
                <Button type="submit">Finish registration</Button>
            </Form>
        </RegistrationVerificationFormStyle>
    );
}
