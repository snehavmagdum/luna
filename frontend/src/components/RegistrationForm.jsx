import {useState} from "react";
import styled from "styled-components";
import {api} from "../axios/index.js";
import Button from "./Button";
import {SubheaderTitle} from "../styles/SubheaderTitle.js";
import {useDispatch, useSelector} from "react-redux";
import {setRegistration} from "../store/slices/user.js";
import {ErrorMessage} from "../styles/ErrorMessage.js";

const RegistrationFormStyle = styled.div`
  display: flex;
  padding-top: 4rem;
  width: 100%;
  height: 80vh;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  gap: 5rem;
  background: #f8f8f8;
`;

const InputStyle = styled.input.attrs({
    type: "email",
})`
  width: 24.1875rem;
  height: 2.8rem;
  flex-shrink: 0;
  border-radius: 0.1875rem;
  border: 1px solid #ebebeb;
  background: #fff;
  padding-left: 1rem;
  outline: none;

  ::placeholder {
    color: #979797;
    font-size: 0.8rem;
    font-style: normal;
    font-weight: 600;
    line-height: normal;
  }

  &:focus {
    border-color: #e47d31;
    box-shadow: 0 0 4px rgba(191, 79, 116, 0.5);
  }
`;

export default function RegistrationForm({handleRegistrationComplete}) {
    const [error, setError] = useState("")
    const dispatch = useDispatch();
    let email = useSelector(state=> state.user.registration.email)

    const handleEmailSubmit = () => {
        // Make an API request to your backend to handle the registration process
        api
            .post("/registration/", {email})
            .then(() => {
                // add email to the user slice
                dispatch(setRegistration({
                    email: email,
                }));

                handleRegistrationComplete();
            })
            .catch((error) => {
                setError(error.response.data)
            });
    };

    return (
        <RegistrationFormStyle>
            <SubheaderTitle>Registration</SubheaderTitle>
            <InputStyle
                placeholder="E-Mail Address"
                value={email}
                onChange={e=> dispatch(setRegistration({email:e.target.value}))}
                required={true}
            />
            <ErrorMessage>{error?.message}</ErrorMessage>
            <Button onClick={handleEmailSubmit}>Register</Button>
        </RegistrationFormStyle>
    );
}
