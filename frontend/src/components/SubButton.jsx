import styled from "styled-components";

const ButtonStyle = styled.button`
  background-color: #e47d31;
  width: 12.5rem;
  height: 40px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  flex-shrink: 0;
  border: none;
  border-radius: 30px;
  color: #fff;
  text-align: center;
  font-size: ${(props) => (props.fontSize ? props.fontSize : "1.25rem")};
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  cursor: pointer;
  text-transform: uppercase;

  &:hover {
    background: #fff;
    border: 2px solid #e47d31;
    color: #e47d31;
  }
`;

export default function SubButton({ children, fontSize, onClick }) {
  return (
    <ButtonStyle fontSize={fontSize} onClick={onClick}>
      {children}
    </ButtonStyle>
  );
}
