import {BrowserRouter, Route, Routes} from "react-router-dom";
import Home from "./pages/Home.jsx";
import Search from "./pages/Search.jsx";
import Profile from "./pages/Profile.jsx";
import Login from "./pages/Login.jsx";
import Register from "./pages/Register.jsx";
import Verification from "./pages/Verification.jsx";
import ProtectedRoutes from "./layouts/ProtectedRoutes.jsx";
import Layout from "./layouts/Layout.jsx";
import CreateRestaurant from "./pages/NewRestaurant.jsx";
import UserSearch from "../components/UserSearch.jsx";
import ReviewSearch from "../components/ReviewSearch.jsx";
import RestaurantSearch from "../components/RestaurantSearch.jsx";
import UserProfileEdit from "../components/UserProfileEdit.jsx";
import UserRestaurantsList from "../components/UserProfileRestaurantsList.jsx";
import UserProfileCommentsList from "../components/UserProfileCommentsList.jsx";
import UserProfileReviewsList from "../components/UserProfileReviewsList.jsx";
import DetailedRestaurant from "./pages/DetailedRestaurant.jsx";
import RestaurantReviewForm from "../components/RestaurantReviewForm.jsx";

const PageRoutes = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route element={<Layout/>}>
                    <Route path="/profile/:user_id" element={<Profile/>}>
                        <Route path="reviews" index element={<UserProfileReviewsList/>}/>
                        <Route path="comments" element={<UserProfileCommentsList/>}/>
                        <Route path="restaurants" element={<UserRestaurantsList/>}/>
                        <Route element={<ProtectedRoutes/>}>
                            <Route path="edit" element={<UserProfileEdit/>}/>
                        </Route>
                    </Route>

                    <Route path="/" element={<Home/>}/>
                    <Route path="/login" element={<Login/>}/>
                    <Route path="/register" element={<Register/>}/>
                    <Route path="/register/verification" element={<Verification/>}/>
                    <Route path="/search" element={<Search/>}>
                        <Route path="/search/restaurants" element={<RestaurantSearch/>}/>
                        <Route path="/search/reviews" element={<ReviewSearch/>}/>
                        <Route path="/search/users" element={<UserSearch/>}/>
                    </Route>
                    <Route
                        path="/restaurants/:restaurantId"
                        element={<DetailedRestaurant/>}
                    ></Route>
                    <Route element={<ProtectedRoutes/>}>
                        <Route
                            path="/restaurants/:restaurantId/review"
                            element={<RestaurantReviewForm/>}
                        />
                        <Route
                            path="/profile/restaurants/new"
                            element={<CreateRestaurant/>}
                        />
                    </Route>

                </Route>
            </Routes>
        </BrowserRouter>
    );
};

export default PageRoutes;
