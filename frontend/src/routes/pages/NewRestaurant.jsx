import {SubheaderTitle} from "../../styles/SubheaderTitle.js";
import styled from "styled-components";
import Button from "../../components/Button.jsx";
import {api} from "../../axios/index.js";
import {useDispatch, useSelector} from "react-redux";
import {useEffect, useState} from "react";
import {ErrorMessage} from "../../styles/ErrorMessage.js";
import {setCategories, setCountries} from "../../store/slices/constants.js";
import {addRestaurant} from "../../store/slices/restaurant.js";
import {useNavigate} from "react-router-dom";

const Main = styled.div`
  background: #F8F8F8;
  height: 140vh;
`;

const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const FieldBox = styled.div`
  display: flex;
  flex-direction: column;
  height: 85.912px;
  justify-content: space-between;
  align-items: start;
  flex-shrink: 0;
`;

const BoxContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(370px, 1fr));
  gap: 30px;
  align-items: center;
  justify-content: center;
  padding: 30px;
`;

const BoxHeader = styled.div`
  padding-top: 30px;
  margin-left: 30px;
  margin-bottom: -20px;
  color: #4C4C4C;
  font-size: 20px;
  font-style: normal;
  font-weight: 700;
  line-height: normal;
`;

const BoxSubHeader = styled.div`
  color: #979797;
  font-size: 20px;
  font-style: normal;
  font-weight: 700;
  line-height: normal;
  width: 100%;
  text-align: left;
`;

const Input = styled.input`
  display: flex;
  width: 100%;
  height: 50px;
  justify-content: center;
  align-items: center;
  flex-shrink: 0;
  align-self: flex-end;
  border: 1px solid #EBEBEB;
  padding-left: 10px;
  margin-bottom: 6px;
`;

const MultipleSelectInput = styled.select`
  display: flex;
  width: 100%;
  height: 50px;
  justify-content: center;
  align-items: center;
  flex-shrink: 0;
  align-self: flex-end;
  border: 1px solid #EBEBEB;
  padding-left: 10px;
  background-color: white; /* Set background color */
  appearance: none; /* Remove default arrow */
  padding-right: 30px; /* Add space for custom arrow */
  background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' fill='%234C4C4C'%3E%3Cpath d='M7 10l5 5 5-5z'/%3E%3C/svg%3E"); /* Add custom arrow */
  background-repeat: no-repeat;
  background-position: right center;
  background-size: 12px;
`;

const CreateBox = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export default function CreateRestaurant() {
    const token = useSelector((state) => state.user.accessToken);
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const categories = useSelector((state) => state.constants.categories)
    const countries = useSelector((state) => state.constants.countries)
    const [error, setError] = useState({})

    useEffect(() => {
        loadCategories();
        loadCountries();
    }, [])

    const loadCategories = async () => {
        try {
            const res = await api
                .get(
                    "/categories/",
                    {
                        headers: {Authorization: "Bearer " + token},
                    }
                )
            dispatch(setCategories(res.data))
        } catch (e) {
            console.log(e)
        }


    }

    const loadCountries = async () => {
        try {
            const res = await api
                .get(
                    "/countries/",
                    {
                        headers: {Authorization: "Bearer " + token},
                    }
                )
            dispatch(setCountries(res.data))
        } catch (e) {
            console.log(e)
        }


    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const formData = new FormData();
        formData.append("name", document.getElementById("restaurantName").value);
        formData.append("category", document.getElementById("category").value);
        formData.append("country", document.getElementById("country").value);
        formData.append("street", document.getElementById("street").value);
        formData.append("city", document.getElementById("city").value);
        formData.append("zip", document.getElementById("zip").value);
        formData.append("website", document.getElementById("website").value);
        formData.append("phone", document.getElementById("phone").value);
        formData.append("email", document.getElementById("email").value);
        formData.append("opening_hours", document.getElementById("openingHours").value);
        formData.append("price_level", document.getElementById("priceLevel").value);

        const imageFile = document.getElementById("image").files[0];

        if (imageFile) {
            formData.append("image", imageFile);
        }

        try {
            const res = await api.post(
                "/restaurants/",
                formData,
                {
                    headers: {Authorization: "Bearer " + token},
                }
            )
            dispatch(addRestaurant(res.data))
            navigate(`/restaurants/${res.data.id}`)
        } catch (e) {
            setError(e.response.data);
        }
    }


    return (
        <Main>
            <MainContainer>
                <SubheaderTitle>Create Restaurant</SubheaderTitle>
            </MainContainer>
            <BoxHeader>Basic</BoxHeader>
            <BoxContainer>
                <FieldBox>
                    <BoxSubHeader>Restaurant Name</BoxSubHeader>
                    <Input id="restaurantName" type="text"/>
                    <ErrorMessage>{error?.name}</ErrorMessage>
                </FieldBox>
                <FieldBox>
                    <BoxSubHeader>Category</BoxSubHeader>
                    <MultipleSelectInput id="category" defaultValue={"swiss"}>
                        {categories.map((category) => (
                            <option key={category.key} value={category.key}>
                                {category.name}
                            </option>
                        ))}
                    </MultipleSelectInput>
                    <ErrorMessage>{error?.category}</ErrorMessage>
                </FieldBox>
                <FieldBox>
                    <BoxSubHeader>Country</BoxSubHeader>
                    <MultipleSelectInput id="country" defaultValue={"CH"}>
                        {countries.map((country) => (
                            <option key={country.key} value={country.key}>
                                {country.name}
                            </option>
                        ))}
                    </MultipleSelectInput>
                    <ErrorMessage>{error?.country}</ErrorMessage>
                </FieldBox>
                <FieldBox>
                    <BoxSubHeader>Street</BoxSubHeader>
                    <Input id="street" type="text"/>
                    <ErrorMessage>{error?.street}</ErrorMessage>
                </FieldBox>
                <FieldBox>
                    <BoxSubHeader>City</BoxSubHeader>
                    <Input id="city" type="text"/>
                    <ErrorMessage>{error?.city}</ErrorMessage>
                </FieldBox>
                <FieldBox>
                    <BoxSubHeader>Zip</BoxSubHeader>
                    <Input id="zip" type="text"/>
                    <ErrorMessage>{error?.zip}</ErrorMessage>
                </FieldBox>
                <FieldBox>
                    <BoxSubHeader>Website</BoxSubHeader>
                    <Input id="website" type="text"/>
                    <ErrorMessage>{error?.website}</ErrorMessage>
                </FieldBox>
                <FieldBox>
                    <BoxSubHeader>Phone</BoxSubHeader>
                    <Input id="phone" type="text"/>
                    <ErrorMessage>{error?.phone}</ErrorMessage>
                </FieldBox>
                <FieldBox>
                    <BoxSubHeader>Email</BoxSubHeader>
                    <Input id="email" type="text"/>
                    <ErrorMessage>{error?.email}</ErrorMessage>
                </FieldBox>
                <FieldBox>
                    <BoxSubHeader>Opening hours</BoxSubHeader>
                    <Input id="openingHours" type="text"/>
                    <ErrorMessage>{error?.opening_hours}</ErrorMessage>
                </FieldBox>
                <FieldBox>
                    <BoxSubHeader>Price level</BoxSubHeader>
                    <MultipleSelectInput id="priceLevel">
                        <option value="">Select a price level</option>
                        <option value="1">$</option>
                        <option value="2">$$</option>
                        <option value="3">$$$</option>
                        <option value="4">$$$$</option>
                    </MultipleSelectInput>
                    <ErrorMessage>{error?.price_level}</ErrorMessage>
                </FieldBox>
                <FieldBox>
                    <BoxSubHeader>Image</BoxSubHeader>
                    <Input id="image" type="file" accept="image/*"/>
                    <ErrorMessage>{error?.image}</ErrorMessage>
                </FieldBox>
            </BoxContainer>
            <CreateBox>
                <Button type="submit" onClick={handleSubmit}>
                    Create
                </Button>
            </CreateBox>
        </Main>
    );
}
