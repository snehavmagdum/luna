import {useDispatch, useSelector} from "react-redux";
import SubheaderNavigation from "../../components/SubheaderNavigation.jsx";
import styled from "styled-components";
import {Outlet} from "react-router-dom";

const SearchContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  margin-bottom: 110px;
`;




export default function Search() {

    const dispatch = useDispatch();

    let searchValue = useSelector((state) => state.search.search);

    return (
        <SearchContainer>
            <SubheaderNavigation/>
            <Outlet/>

        </SearchContainer>
       /* <div>
            <h1>Search</h1>
            <h2>{searchValue}</h2>
        </div>*/
    )
}