import RestaurantCard from "../../components/RestaurantCard.jsx";
import HomeSearch from "../../components/HomeSearch.jsx";
import { SubheaderTitle } from "../../styles/SubheaderTitle.js";
import styled from "styled-components";
import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {setRestaurants} from "../../store/slices/restaurant.js";
import Loading from "../../components/Loading.jsx";
import {api} from "../../axios/index.js";

const HomeContainer = styled.div`
  background: #f8f8f8;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  gap: 2rem;
  margin-bottom: 110px;
`;

const CardContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  gap: 30px;
  margin-top: 42px;
  margin-bottom: 100px;
`;

export default function Home() {
    const [loading, setLoading] = useState(true)
    const dispatch = useDispatch()
    const bestRestaurants = useSelector(state => state.restaurant.restaurants)

    useEffect(() => {
        loadBestRestaurants();
    }, [])

    const loadBestRestaurants = async () => {
        try {
            const res = await api.get("/home")
            dispatch(setRestaurants(res.data))
            setLoading(false)
        } catch (e) {
            console.log(e)
        }
    }

    if (loading) {
        return <Loading/>
    } else {
        return (
            <HomeContainer>
                <HomeSearch/>
                <SubheaderTitle>Best rated restaurants</SubheaderTitle>
                <CardContainer>
                    {bestRestaurants.map((restaurant)=> {
                        return <RestaurantCard key={restaurant.id} data={restaurant}/>
                    })}
                </CardContainer>
            </HomeContainer>
        )
    }
}