import UserProfileNavigation from "../../components/UserProfileNavigation";
import styled from "styled-components";
import { UserProfileBanner } from "../../styles/UserProfileBanner";
import UserProfileShortInfo from "../../components/UserProfileShortInfo";
import UserProfileInfo from "../../components/UserProfileInfo";
import { Outlet, useParams } from "react-router-dom";
import { setSelectedProfile } from "../../store/slices/profile";
import { useSelector, useDispatch } from "react-redux";
import { api } from "../../axios";
import { useEffect, useState } from "react";
import Loading from "../../components/Loading.jsx";
import UserProfileAvatar from "../../components/UserProfileAvatar.jsx";

const UserProfileWrapperStyle = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  flex-direction: column;
  flex-shrink: 0;
  margin-bottom: 110px;
`;

const ProfileWrapperStyle = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
  align-items: top;
  padding-left: 60px;
  padding-right: 60px;
  margin-top: 48px;
`;

const LeftSide = styled.div`
  width: 230px;
  display: flex;
  align-items: start;
  gap: 1rem;
  flex-direction: column;
  margin-right: 14px;
`;
const RightSide = styled.div`
  display: grid;
  flex-grow: 1;
  align-items: left;
  gap: 1rem;
  flex-direction: column;
`;
const Bottom = styled.div`
  display: flex;
  align-items: flex-start;
  gap: 1rem;
`;

const ProfileText = styled.p`
  color: #4c4c4c;
  font-size: 1.125rem;
  font-style: normal;
  font-weight: 700;
  line-height: normal;
`;

export default function Profile() {
  const profile = useSelector((state) => state.profile.selectedProfile);
  const token = useSelector((state) => state.user.accessToken);
  const { user_id } = useParams();
  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch();

  const loadUser = async () => {
    let headers = {};
    if (token) {
      headers = { Authorization: `Bearer ${token}` };
    }
    try {
      const res = await api.get(`users/${user_id}`, { headers });
      dispatch(setSelectedProfile(res.data));
      setLoading(false);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    loadUser();
  }, []);

  if (loading) {
    return <Loading />;
  }

  const {
    first_name,
    last_name,
    location,
    registered_date,
    things_liked,
    num_reviews,
    num_comments,
    description,
  } = profile;

  return (
    <UserProfileWrapperStyle>
      <UserProfileBanner src="/bannerProfileDefault.jpg"></UserProfileBanner>
      <ProfileWrapperStyle>
        <LeftSide>
          <UserProfileAvatar />
          <ProfileText>{first_name}’s profile</ProfileText>
          <UserProfileNavigation />
        </LeftSide>

        <RightSide>
          <UserProfileShortInfo
            name={`${first_name} ${last_name[0]}.`}
            location={location ? location : "Somewhere on earth"}
            num_of_reviews={num_reviews}
            num_of_comments={num_comments}
          />

          <Bottom>
            <Outlet />
            <UserProfileInfo
              first_name={first_name}
              location={location}
              member_since={registered_date}
              things_i_love={things_liked}
              description={description}
            />
          </Bottom>
        </RightSide>
      </ProfileWrapperStyle>
    </UserProfileWrapperStyle>
  );
}
