import { useState } from "react";
import RegistrationForm from "../../components/RegistrationForm.jsx";
import RegistrationConfirmation from "../../components/RegistrationConfirmation.jsx";
import RegistrationVerificationForm from "../../components/RegistrationVerificationForm.jsx";

export default function Register() {
  const [registrationStep, setRegistrationStep] = useState("");

  const handleRegistrationComplete = () => {
    console.log("Registration complete");
    setRegistrationStep("confirmation");
  };

  const handleRegistrationConfirmationComplete = () => {
    console.log("Registration confirmation complete");
    setRegistrationStep("verification");
  };

  if (registrationStep === "confirmation") {
    return (
      <RegistrationConfirmation
        handleRegistrationConfirmationComplete={handleRegistrationConfirmationComplete}
      />
    );
  } else if (registrationStep === "verification") {
    return <RegistrationVerificationForm />;
  } else {
    // state unclear or "registration"
    return <RegistrationForm handleRegistrationComplete={handleRegistrationComplete} />;
  }
}
