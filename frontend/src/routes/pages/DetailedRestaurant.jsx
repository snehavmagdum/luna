import {useNavigate, useParams} from "react-router-dom";
import {api} from "../../axios/index.js";
import styled from "styled-components";
import SubButton from "../../components/SubButton.jsx";
import StarRating from "../../components/StarRating.jsx";
import {useState, useEffect} from "react";
import RestaurantReview from "../../components/RestaurantReview.jsx";
import RestaurantBanner from "../../components/RestaurantBanner.jsx";
import {useDispatch, useSelector} from "react-redux";
import {setSelectedRestaurant, setSelectedRestaurantReviews} from "../../store/slices/restaurant.js";
import Loading from "../../components/Loading.jsx";

const Main = styled.div`
  height: 2000px;
  background: #F8F8F8;
  margin-bottom: 100px;
`;

const InformationTab = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: center;
  background-color: #F5F5F5;
`;

const Comments = styled.div`
  height: 100%;
  margin-left: 20px;
  padding-top: 20px;
  width: 45%;
`;
const StoreInfo = styled.div`
  margin: 20px;
  padding: 20px;
  width: 45%;
  background-color: #f1f1f1;
  height: 1000px;
`;


const FilterBox = styled.div`
  margin-top: 20px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  height: 40px;
  margin-bottom: 20px;
`;

const Input = styled.input`
  color: #979797;
  font-size: 16px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  width: 100%;
  margin-right: 20px;
`;

const CommentsBox = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`;

const ReviewCount = styled.div`
  color: #FFF;
  text-align: center;
  font-size: 20px;
  font-style: normal;
  font-weight: 300;
  line-height: normal;
`;

const Image = styled.div`
  width: 100%;
  height: 100%;
`;

const RestaurantName = styled.div`
  color: #FFF;
  font-size: 30px;
  font-style: normal;
  font-weight: 700;
  line-height: normal;
`;

const RestaurantType = styled.div`
  color: #FFF;
  text-align: center;
  font-size: 24px;
  font-style: normal;
  font-weight: 300;
  line-height: normal;
`;

const Reviews = styled.div`
  display: flex;
  flex-direction: column;
`;

const Text = styled.div`
  color: #000;
  font-size: 20px;
  font-style: normal;
  font-weight: 300;
  line-height: normal;
  width: 100%;
  height: 40px;
  border-bottom: #767676 1px;
`;

const CheckoutButtons = styled.div`
  display: flex;
  justify-content: center;
  margin-right: 10%;
  margin-top: 20px;
  margin-left: 10%;
`;

export default function DetailedRestaurant() {
    const {restaurantId} = useParams();
    const restaurant = useSelector(state => state.restaurant.selectedRestaurant)
    const restaurantReviews = useSelector(state => state.restaurant.selectedRestaurantReviews)
    const navigate = useNavigate();
    const dispatch = useDispatch()
    const [loading, setLoading] = useState(true)

    const fetchRestaurantData = async () => {
        try {
            // Fetch comments from the server or any other source
            const restaurantResponse = await api.get(`/restaurants/${restaurantId}`);
            dispatch(setSelectedRestaurant(restaurantResponse.data))

            const reviewsResponse = await api.get(`/reviews/restaurant/${restaurantId}`);
            dispatch(setSelectedRestaurantReviews(reviewsResponse.data))

            setLoading(false)

        } catch (error) {
            console.error("Error fetching restaurant and/or review data:", error);
        }
    };

    useEffect(() => {
        fetchRestaurantData();
    }, [restaurantId]);

    if (loading) {
        return <Loading/>
    } else {

        return (
            <Main>
                <RestaurantBanner isLarge data={restaurant}>
                    <Image src={restaurant.image ? restaurant.image : ""}/>
                    <RestaurantName>{restaurant.name}</RestaurantName>
                    <RestaurantType>{restaurant.category}</RestaurantType>
                    <Reviews>
                        <StarRating/>
                        <ReviewCount>{restaurant.num_reviews} Reviews</ReviewCount>
                    </Reviews>
                </RestaurantBanner>

                <InformationTab>
                    <Comments>
                        <CommentsBox>
                            {/* Render comments here */}
                            {restaurantReviews.length > 0 ? (
                                restaurantReviews.map((review) => (
                                    <div key={review.id} id={`review-${review.id}`}>
                                        <RestaurantReview review={review}/>
                                    </div>
                                ))
                            ) : (
                                <Text>No reviews yet!</Text>
                            )}
                        </CommentsBox>


                    </Comments>
                    <StoreInfo>
                        <Text>Opening hours: {restaurant.opening_hours}</Text>
                        <Text>Price
                            Level: {restaurant.price_level === 1 ? "$" : restaurant.price_level === 2 ? "$$" : restaurant.price_level === 3 ? "$$$" : "$$$"}</Text>
                        <CheckoutButtons>
                            <SubButton onClick={() => navigate(`/restaurants/${restaurant.id}/review`)}>Write a
                                review</SubButton>
                        </CheckoutButtons>
                    </StoreInfo>
                </InformationTab>
            </Main>
        );
    }
}
