import Header from "../../components/Header.jsx";
import {Outlet} from "react-router-dom";
import Footer from "../../components/Footer.jsx";

export default function Layout() {

    // some other code here

    return (<>
        <Header/>
        <Outlet/>
        <Footer/>
    </>);
}


