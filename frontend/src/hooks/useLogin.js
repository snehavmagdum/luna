import {useState} from "react";
import {useNavigate} from "react-router-dom";
import {useDispatch} from "react-redux";
import {loadUserDetails, login, logout} from "../store/slices/user";
import {api} from "../axios";

const useLogin = (email, password) => {
    const [loginError, setLoginError] = useState("");
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleLogin = async (e) => {
        setLoginError("");
        try {
            const res = await api.post("auth/token/", {
                email,
                password,
            });
            const token = res.data.access;
            dispatch(login(token));
            localStorage.setItem("token", token);


            const resUser = await api.get("me/", {
                headers: {Authorization: `Bearer ${token}`}
            });

            dispatch(loadUserDetails(resUser.data))
            navigate(`/profile/${resUser.data.id}/reviews`);

        } catch (error) {
            if (error.response) {
                setLoginError(error.response.data);
            } else if (error.request) {
                console.log(error.request);
            } else {
                console.log("Error", error.message);
            }
        }
    };


    const handleLogout = () => {
        localStorage.removeItem("token");
        dispatch(logout());
        navigate("/login");
    };


    return {
        handleLogin,
        handleLogout,
        loginError,
    };
};

export default useLogin;
