import styled from "styled-components";

export const ErrorMessage = styled.span`
  color: indianred;
  padding-left: 4px;
`