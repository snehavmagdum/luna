import styled from "styled-components";

export const UserProfileBanner = styled.img`
  width: 100%;
  height: 9.6875rem;
  object-fit: cover;
  position: absolute;
  z-index: -1;
`;
