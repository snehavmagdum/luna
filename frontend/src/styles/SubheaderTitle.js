import styled from "styled-components";

export const SubheaderTitle = styled.h1`
  position: relative;
  text-transform: uppercase;

  &:after {
    content: "";
    position: absolute;
    top: 80%;
    left: 50%;
    transform: translateX(-50%);
    width: 70%;
    height: 3px;
    background-color: #f07c3e;
    margin-top: 1rem;
  }
`;
