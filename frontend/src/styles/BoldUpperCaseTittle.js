import styled from "styled-components";

export const BoldUpperCaseTittle = styled.h1`
  color: #303030;
  font-size: 1.25rem;
  font-style: normal;
  font-weight: 700;
  line-height: normal;
  text-transform: uppercase;
  margin-bottom: 1rem;
`;
